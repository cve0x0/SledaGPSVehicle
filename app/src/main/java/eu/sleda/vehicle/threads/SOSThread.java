package eu.sleda.vehicle.threads;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Handler;

import java.io.File;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.helpers.AudioRecorder;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.camera.Camera;
import eu.sleda.vehicle.helpers.camera.CameraTypes;
import eu.sleda.vehicle.helpers.camera.interfaces.OnImageResult;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.Message;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class SOSThread extends Thread {
    private ModuleMessages messages = ModuleMessages.instance();
    private String directory = Utils.createAppDirectory("SOSThread");
    private Camera camera;
    private Context context;
    private Handler handler = new Handler();

    public SOSThread(Context context) {
        this.context = context;
    }

    private void recordAudio() {
        final File file = new File(directory + System.currentTimeMillis() + ".mp3");

        final AudioRecorder audioRecorder = new AudioRecorder(file.getAbsolutePath());

        audioRecorder.start();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                audioRecorder.stop();
                Message message = new Message("");
                message.setFile(file, MessagesTypes.MESSAGE_VOICE);
                messages.writeMessage(message);
            }
        }, 60 * 1000);
    }

    private void cameraCapture(final byte cameraType) {
        File file = new File(directory + System.currentTimeMillis() + ".jpeg");

        camera.setCameraType(cameraType);
        camera.takePicture(new OnImageResult() {
            @Override
            public void onResult(Bitmap image, File output) {
                Logger.d("SOSThread image capture success");

                Message message = new Message("");
                message.setFile(output, MessagesTypes.MESSAGE_IMAGE);
                messages.writeMessage(message);
            }

            @Override
            public void onError() {
                Logger.e("SOSThread image capture e");
            }

            @Override
            public void onReady() {
                if (cameraType == CameraTypes.BACK) cameraCapture(CameraTypes.FRONT);
            }
        }, file);
    }

    @Override
    public void run() {
        Logger.d("SOSThread() Activated");

        messages.writeMessage(new Message("SOSThread"));

        recordAudio();

        camera = new Camera(context, handler);

        cameraCapture(CameraTypes.BACK);

        MediaPlayer.create(context, R.raw.message).start();
    }
}
