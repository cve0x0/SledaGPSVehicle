package eu.sleda.vehicle.modules;

import android.util.SparseArray;

import eu.sleda.vehicle.configs.Protocol;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.PackageBuffer;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleRequest;
import eu.sleda.vehicle.receivers.TemperatureReceiver;
import eu.sleda.vehicle.services.LocationTrackingService;

/**
 * Created by Cvetomir Marinov
 */

public class ModuleDetails implements Module, ModuleRequest {
    private LocationTrackingService locationTrackingService = LocationTrackingService.instance();
    private int lastSpeed = -100;
    private byte lastTemperature = -100;

    public ModuleDetails() {
        Logger.d("ModuleDetails() -> create instance");
    }

    @Override
    public void onRequest(PackageBuffer buf) {
        Logger.d("ModuleDetails::onRequest()");

        int speed = locationTrackingService.hasSpeed() ? locationTrackingService.getSpeed() : -100;
        byte temperature = TemperatureReceiver.getInstance().getTemperature();

        if (speed != lastSpeed || temperature != lastTemperature) {
            buf.writeByte(Protocol.SEND_DETAILS);

            if(speed != lastSpeed) {
                buf.writeByte(Protocol.SEND_GPS_SPEED);
                buf.writeInt(lastSpeed = speed);
            } else {
                buf.writeByte(Protocol.NULL);
            }

            buf.writeByte((lastTemperature = temperature));
        }
    }

    @Override
    public void onRequestSuccess(SparseArray<Object> options) {

    }
}
