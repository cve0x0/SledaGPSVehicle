package eu.sleda.vehicle.modules;

import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.regex.Matcher;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.activities.MainActivity;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.ConnectionTypes;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.configs.Protocol;
import eu.sleda.vehicle.fragments.DispatcherTabFragment;
import eu.sleda.vehicle.helpers.LocationParser;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.PackageBuffer;
import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleRegularlyRequest;
import eu.sleda.vehicle.interfaces.ModuleRequest;
import eu.sleda.vehicle.objects.GEOLocation;
import eu.sleda.vehicle.objects.Message;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class ModuleMessages implements Module, ModuleRequest, ModuleRegularlyRequest {
    private ArrayList<Message> messagesQueue = new ArrayList<>();
    private Watcher watcher = Watcher.getInstance();
    private EasyTCPClient conn = watcher.getConnection();
    private static ModuleMessages instance;
    private SmsManager smsManager = SmsManager.getDefault();

    public ModuleMessages() {
        Logger.d("ModuleMessages() -> Create instance");
        instance = this;
    }

    public static ModuleMessages instance() {
        return instance;
    }

    private void addQueueMessage(Message message) {
        Logger.d("ModuleMessages::addQueueMessage() -> " + message.getMessage());
        message.setSuccess(false);
        messagesQueue.add(message);
        DispatcherTabFragment.instance().onMessageAdd(message);
    }

    public void putQueueMessage(Message message) {
        messagesQueue.add(message);
    }

    public void writeMessage(final Message message) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    writeMessage(message);
                }
            });
            return;
        }

        Logger.d("ModuleMessages::writeMessage() -> Connection Type: " + Utils.getConnectionType());

        if (!message.isFileAvailable() && Utils.getConnectionType() == ConnectionTypes.SMS) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            writeSMS(message);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            addQueueMessage(message);
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getContext());
            builder.setTitle(watcher.getString(R.string.confirm_question))
                    .setMessage(watcher.getString(R.string.modulemessages_sms_send))
                    .setPositiveButton(watcher.getString(R.string.positive), dialogClickListener)
                    .setNegativeButton(watcher.getString(R.string.negative), dialogClickListener)
                    .show();
            return;
        }

        addQueueMessage(message);
    }

    private void writeSMS(Message message) {
        Logger.d("ModuleMessages::writeSMS()");
        smsManager.sendTextMessage(Config.PHONE, null, message.getMessage() + "-" + watcher.user.getUserId(), null, null);
        message.setIsSMS(true);
        DispatcherTabFragment.instance().onMessageAdd(message);
    }

    public void onMessageReceive(Message message) {
        message.setType(MessagesTypes.MESSAGE_RECEIVED);
        DispatcherTabFragment.instance().onMessageReceive(message);

        // TODO: Need to be move to other function
        try {
            Matcher matcher = LocationParser.parse(message.getMessage());
            while (matcher.find()) {
                String data = matcher.group(2);
                ArrayList<GEOLocation> locations = new ArrayList<>();

                for (String coordinate : data.split(",")) {
                    String[] info = coordinate.split(";");

                    if (info.length != 2) {
                        continue;
                    }

                    locations.add(new GEOLocation(Double.parseDouble(info[0]), Double.parseDouble(info[1])));
                }

                ModuleLocation.getInstance().addAddressLocations(locations);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Logger.d("ModuleMessages::onMessageReceive() -> Message: " + message.getMessage() + ", Date: " + message.getDate());
    }

    @Override
    public void onRegularlyRequest(PackageBuffer buf) {
        Logger.d("ModuleMessages::onRegularlyRequest()");

        if (messagesQueue.size() > 0) {
            buf.requireResponse = true;

            buf.writeByte(Protocol.SEND_MESSAGES);
            buf.writeByte((byte) messagesQueue.size());

            ArrayList<Message> messages = new ArrayList<>(messagesQueue);

            for (Message message: messages) {
                buf.writeChunkString(message.getMessage());

                if (message.isFileAvailable()) {
                    buf.writeByte(message.getFileType());
                    buf.writeFile(message.getFile());
                } else {
                    buf.writeByte(Protocol.SEND_MESSAGE_WITHOUT_FILE);
                }
            }

            buf.options.append(Protocol.SEND_MESSAGES, messages);
        }
    }

    public void readMessages() {
        int i = conn.readUnsignedByte(), lastDate = 0;
        ModuleMessages messages = ModuleMessages.instance();

        while (i-- > 0) {
            String text = conn.readChunkString();
            int date = (lastDate = conn.readInt());

            if (conn.err)
                break;

            messages.onMessageReceive(new Message(text, date * 1000));
        }

        if (!conn.err) {
            PackageBuffer buf = new PackageBuffer();
            buf.writeByte(Protocol.MESSAGES_RECEIVED_SUCCESS);
            buf.writeInt(lastDate);

            conn.writeBytes(buf.toByteArray());
            conn.flush();
        }
    }

    @Override
    public void onRequest(PackageBuffer buf) {

    }

    @Override
    public void onRequestSuccess(SparseArray<Object> options) {
        ArrayList<Message> messages = (ArrayList<Message>) options.get(Protocol.SEND_MESSAGES);

        if (messages != null) {
            for (Message message: messages) {
                message.setSuccess(true);
                DispatcherTabFragment.instance().messagesDatabase.updateMessage(message);
                messagesQueue.remove(message);
            }
        }
    }
}
