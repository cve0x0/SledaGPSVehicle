package eu.sleda.vehicle.modules;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.util.Log;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleLogout;
import eu.sleda.vehicle.receivers.HeadsetReceiver;
import eu.sleda.vehicle.receivers.SMSReceiver;
import eu.sleda.vehicle.receivers.TemperatureReceiver;
import eu.sleda.vehicle.services.LocationTrackingService;

/**
 * Created by Cvetomir Marinov
 */

public class ModuleServices implements Module, ModuleLogout {
    private Context context = Watcher.getContext().getApplicationContext();
    private PowerManager.WakeLock wakeLock;

    public ModuleServices() {
        Logger.d("ModuleServices() -> create instance");

        context.registerReceiver(new TemperatureReceiver(), new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        context.registerReceiver(new HeadsetReceiver(), new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        context.registerReceiver(new SMSReceiver(), new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        context.startService(new Intent(context, LocationTrackingService.class));

        keepCPU();
    }

    private void keepCPU() {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WakeLock");
        if ((wakeLock != null) && (!wakeLock.isHeld())) {
            wakeLock.acquire();
        }
    }

    @Override
    public void onLogout() {
        Logger.d("ModuleServices::onLogout()");

        try {
            context.stopService(new Intent(context, LocationTrackingService.class));
            context.unregisterReceiver(TemperatureReceiver.getInstance());
            context.unregisterReceiver(HeadsetReceiver.getInstance());
            context.unregisterReceiver(SMSReceiver.getInstance());
            wakeLock.release();
        } catch (Exception ex) {
            Logger.e(Log.getStackTraceString(ex));
        }
    }
}
