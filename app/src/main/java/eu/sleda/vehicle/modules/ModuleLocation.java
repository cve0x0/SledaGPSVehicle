package eu.sleda.vehicle.modules;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.configs.Protocol;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.PackageBuffer;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleRequest;
import eu.sleda.vehicle.objects.GEOLocation;
import eu.sleda.vehicle.objects.LocationAddress;
import eu.sleda.vehicle.services.LocationTrackingService;

/**
 * Created by Cvetomir Marinov
 */

public class ModuleLocation implements Module, ModuleRequest {

    private static ModuleLocation instance;
    private LocationTrackingService locationTrackingService = LocationTrackingService.instance();

    public ModuleLocation() {
        Logger.d("ModuleLocation() -> create instance");
        instance = this;
    }

    public static ModuleLocation getInstance() {
        return instance;
    }

    @Override
    public void onRequest(PackageBuffer buf) {
        Logger.d("ModuleLocation::onRequest()");

        ArrayList<Location> copyLocations = locationTrackingService.getLocationsHistory(false);

        if (copyLocations.size() > 0) {
            buf.requireResponse = true;

            buf.writeByte(Protocol.SEND_LOCATIONS);
            buf.writeByte((byte) copyLocations.size());

            for (Location location : copyLocations) {
                float lat = Float.parseFloat(Location.convert(location.getLatitude(), Location.FORMAT_DEGREES).replace(',', '.'));
                float lng = Float.parseFloat(Location.convert(location.getLongitude(), Location.FORMAT_DEGREES).replace(',', '.'));
                int date = (int) (location.getTime() / 1000);

                Logger.d("ModuleLocation::ModuleLocation() -> Send Location: " + lat + " - " + lng);

                buf.writeFloat(lat);
                buf.writeFloat(lng);
                buf.writeInt(date);
            }

            buf.options.append(Protocol.SEND_LOCATIONS, copyLocations);
        }
    }

    @Override
    public void onRequestSuccess(SparseArray<Object> options) {
        ArrayList<Location> locations_success = (ArrayList<Location>) options.get(Protocol.SEND_LOCATIONS);
        if (locations_success != null) {
            ArrayList<Location> locations = locationTrackingService.getLocationsHistory(true);

            for (Location location: locations_success) {
                locations.remove(location);
            }
        }
    }

    public void addAddressLocations(ArrayList<GEOLocation> locations) {
        try {
            Geocoder geocoder = new Geocoder(Watcher.getContext(), Locale.getDefault());

            for (GEOLocation location : locations) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                List<Address> addressList = null;

                try {
                    addressList = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (Exception e) {
                    Logger.e(e.getMessage());
                    Logger.e(e.toString());

                    e.printStackTrace();
                }

                String address = latitude + "; " + longitude;
                if (addressList != null && !addressList.isEmpty()) {
                    address = addressList.get(0).getAddressLine(0);
                }

                LocationAddress locationAddress = new LocationAddress(location.getLatitude(), location.getLongitude(), address);
                Watcher.getInstance().getMessagesDatabase().insertAddressLocation(locationAddress);
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }
    }

    public boolean sygicNavigate(ArrayList<GEOLocation> locations, Context context) {
        try {
            String locationsOutput = "";

            for (GEOLocation location: locations) {
                locationsOutput += location.getLatitude() + "|" + location.getLongitude() + "|";
            }

            Logger.d("ModuleLocation::sygicNavigate() -> " + locationsOutput);

            String navigate = "com.sygic.aura://coordinate|" + locationsOutput + "drive";

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(navigate));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

            return true;
        } catch (Exception e) {
            e.printStackTrace();

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.sygic.aura"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.sygic.aura"));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }

            return false;
        }
    }
}
