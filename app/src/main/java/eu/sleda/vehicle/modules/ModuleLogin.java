package eu.sleda.vehicle.modules;

import java.util.HashMap;

import eu.sleda.vehicle.activities.LaunchActivity;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.ConnectionTypes;
import eu.sleda.vehicle.configs.Protocol;
import eu.sleda.vehicle.helpers.Updater;
import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;
import eu.sleda.vehicle.interfaces.OnLogin;
import eu.sleda.vehicle.receivers.NetworkChangeReceiver;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class ModuleLogin implements Runnable {
    private OnLogin onLogin;
    private String phone, PIN;
    private EasyTCPClient conn = new EasyTCPClient();
    private Updater updater;

    public ModuleLogin(OnLogin onLogin, Updater updater, String phone, String PIN) {
        this.onLogin = onLogin;
        this.updater = updater;
        this.phone = phone;
        this.PIN = PIN;
    }

    private boolean openConnection() {
        if(conn.isConnected())
            return true;
        conn.close();
        conn = new EasyTCPClient(Config.SERVER, Config.PORT, Config.SOCKET_CONNECT_TIMEOUT*1000);
        conn.setTimeout(Config.SOCKET_READ_TIMEOUT*1000);
        conn.streamFlush(false);
        return conn.isConnected();
    }

    public EasyTCPClient getConnection() {
        return conn;
    }

    public void run() {
        if(!NetworkChangeReceiver.getInstance().isNetworkAvailable()) {
            onLogin.onNetworkNotAvailable();
            return;
        }

        if(!openConnection()) {
            onConnectionError();
            return;
        }

        conn.writeByte(Protocol.LOGIN);
        conn.flush();

        int version = getVersion();

        if (updater.isUpdateAvailable(version)) {
            if (updater.isUpdateRequired(version) || Utils.getConnectionType() == ConnectionTypes.WIFI) {
                conn.close();
                onLogin.onUpdate(version);
                return;
            }
        }

        conn.writeLine(phone);
        conn.writeLine(PIN);
        conn.flush();

        byte status = conn.readByte();

        if(status == Protocol.LOGIN_SUCCESS) {
            onSuccessLogin();
        } else if(status == Protocol.LOGIN_INVALID) {
            onLogin.onInvalidDetails();
        } else {
            onConnectionError();
        }
    }

    int getVersion() {
        return conn.readInt();
    }

    private void onConnectionError() {
        conn.close();

        onLogin.onConnectionError();
    }

    private void onSuccessLogin() {
        conn.writeLine(Utils.getIMEI(LaunchActivity.getContext()));
        conn.flush();

        final HashMap<String, Object> details = new HashMap<>();
        details.put("userID", conn.readInt());
        details.put("token", conn.readLine());
        details.put("phone", phone);

        conn.close();

        onLogin.onSuccess(details);
    }
}
