package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleHeadset {
    void onHeadsetPlug(byte state);
}
