package eu.sleda.vehicle.interfaces;

import java.io.IOException;

public interface OnBytesRead {
    void onRead(byte[] buff, int size) throws IOException;
}
