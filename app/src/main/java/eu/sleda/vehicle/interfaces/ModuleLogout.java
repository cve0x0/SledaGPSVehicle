package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleLogout {
    void onLogout();
}
