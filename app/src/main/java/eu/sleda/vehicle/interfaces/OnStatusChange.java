package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface OnStatusChange {
    void onChange(boolean status);
}
