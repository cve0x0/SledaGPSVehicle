package eu.sleda.vehicle.interfaces;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Cvetomir Marinov
 */

public interface OnDatabaseCreated {
    void onCreated(SQLiteDatabase db);
}
