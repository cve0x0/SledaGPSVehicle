package eu.sleda.vehicle.interfaces;

import java.util.HashMap;

/**
 * Created by Cvetomir Marinov
 */

public interface OnLogin {
    void onSuccess(HashMap<String, Object> details);
    void onInvalidDetails();
    void onNetworkNotAvailable();
    void onConnectionError();
    void onUpdate(int version);
}
