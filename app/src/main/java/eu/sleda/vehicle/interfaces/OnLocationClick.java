package eu.sleda.vehicle.interfaces;

import java.util.ArrayList;

import eu.sleda.vehicle.objects.GEOLocation;

/**
 * Created by Cvetomir Marinov
 */

public interface OnLocationClick {
    void onClick(ArrayList<GEOLocation> locations);
}
