package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleNetworkChange {
    void onNetworkChange(boolean status, int state);
}
