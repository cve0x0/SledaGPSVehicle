package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface OnTimeUpdate {
    void onTimeUpdate(byte minutes, byte seconds);
}
