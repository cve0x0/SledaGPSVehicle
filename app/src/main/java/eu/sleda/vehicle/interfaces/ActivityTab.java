package eu.sleda.vehicle.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by Cvetomir Marinov
 */

public interface ActivityTab {
    String getTitle();
    Fragment getFragment();
    int getIcon();
}
