package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleSpeed {
    void onSpeedChange(int speed);
}
