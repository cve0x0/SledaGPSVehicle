package eu.sleda.vehicle.interfaces;

import eu.sleda.vehicle.objects.Carrier;

/**
 * Created by Cvetomir Marinov
 */

public interface ModulePhoneStateChange {
    void onStateChange(int state, Carrier carrier);
}
