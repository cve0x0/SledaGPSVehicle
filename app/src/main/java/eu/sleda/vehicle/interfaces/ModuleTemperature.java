package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleTemperature {
    void onTemperatureChange(byte temperature);
}
