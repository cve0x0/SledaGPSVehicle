package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface OnHttpCallback {
    void onReady(String response);
}
