package eu.sleda.vehicle.interfaces;

import eu.sleda.vehicle.helpers.PackageBuffer;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleRegularlyRequest {
    void onRegularlyRequest(PackageBuffer buf);
}
