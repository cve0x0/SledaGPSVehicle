package eu.sleda.vehicle.interfaces;

import android.util.SparseArray;

import eu.sleda.vehicle.helpers.PackageBuffer;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleRequest {
    void onRequest(PackageBuffer buf);
    void onRequestSuccess(SparseArray<Object> options);
}
