package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface WakeUp extends Runnable {
    void onSuccessReceive(byte packageID);
    void onRequest(boolean regularly);
}
