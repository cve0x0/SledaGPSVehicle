package eu.sleda.vehicle.interfaces;

/**
 * Created by Cvetomir Marinov
 */

public interface SettingOnSwitch {
    void onSwitch(boolean status);
    boolean onLoad();
}
