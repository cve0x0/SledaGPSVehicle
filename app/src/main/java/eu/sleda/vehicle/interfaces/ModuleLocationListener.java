package eu.sleda.vehicle.interfaces;

import android.location.Location;

/**
 * Created by Cvetomir Marinov
 */

public interface ModuleLocationListener {
    void onLocationChange(Location location);
}
