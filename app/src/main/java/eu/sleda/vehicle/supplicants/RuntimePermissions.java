package eu.sleda.vehicle.supplicants;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import eu.sleda.vehicle.helpers.Logger;

/**
 * Created by Viliyan Vasilev
 */

public class RuntimePermissions {

    private static String[] sRuntimePermissions = {
            Manifest.permission.CAMERA,

            Manifest.permission.INTERNET,

            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.CHANGE_NETWORK_STATE,

            Manifest.permission.WAKE_LOCK,

            Manifest.permission.VIBRATE,

            Manifest.permission.READ_PHONE_STATE,

            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,

            Manifest.permission.RECORD_AUDIO,

            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,

            Manifest.permission.READ_SMS,
            Manifest.permission.SEND_SMS,

            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.RECEIVE_MMS,

            Manifest.permission.RECEIVE_BOOT_COMPLETED
    };

    public static boolean checkPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                for (String permission : sRuntimePermissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_DENIED) {
                        return false;
                    }
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }
        }

        return true;
    }

    public static void requestPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                ActivityCompat.requestPermissions((Activity) context, sRuntimePermissions, 1);
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }
        }
    }
}