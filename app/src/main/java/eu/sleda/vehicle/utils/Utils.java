package eu.sleda.vehicle.utils;

import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.ConnectionTypes;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.listeners.StateListener;
import eu.sleda.vehicle.receivers.NetworkChangeReceiver;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class Utils {
    public static int getInternetConnectivityStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo isNetworkActive = null;
        if (connectivityManager != null) {
            isNetworkActive = connectivityManager.getActiveNetworkInfo();
        }

        if (isNetworkActive != null) {
            return isNetworkActive.getType();
        }

        return -1;
    }

    public static int getConnectionType() {
        StateListener phoneState = StateListener.getInstance();
        NetworkChangeReceiver network = NetworkChangeReceiver.getInstance();

        if (network.isWiFi()) {
            return ConnectionTypes.WIFI;
        } else if (phoneState.isConnected()) {
            if (!network.isNetworkAvailable() || phoneState.isBlocked()) {
                return ConnectionTypes.SMS;
            } else {
                return ConnectionTypes.MOBILE;
            }
        } else {
            return ConnectionTypes.NOT_AVAILABLE;
        }
    }

    public static boolean isConnectionReady() {
        int type = getConnectionType();

        return type == ConnectionTypes.WIFI || type == ConnectionTypes.MOBILE;
    }

    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String IMEI = telephonyManager.getDeviceId();

        return IMEI != null ? IMEI : "0000";
    }

    public static String getExternal() {
        return Environment.getExternalStorageDirectory().toString();
    }

    public static String getHomeDirectory() {
        return getExternal() + "/" + Config.HOME_DIRECTORY + "/";
    }

    public static String createAppDirectory(String dir) {
        File file = new File(getHomeDirectory() + dir + "/");

        boolean isAppDirectoryCreated = false;

        try {
            isAppDirectoryCreated = file.mkdirs();
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }

        if (isAppDirectoryCreated && file.exists() && file.isDirectory()) {
            Logger.i("Application directory created");

            return file.toString();
        } else if (file.exists() && file.isDirectory()) {
            Logger.i("Application directory exists");

            return file.toString();
        } else{
            Logger.w("Application directory not created and not exists");

            return null;
        }
    }

    public static void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }
    }

    public static String getVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);

            return pInfo.versionName;
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }

        return null;
    }

    public static String getLauncherPackageName(Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        ResolveInfo resolveInfo = context.getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);

        return resolveInfo.activityInfo.packageName;
    }

    public static boolean isAppLockerActivated(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        return sharedPreferences.contains(Config.PREFERENCE_CODE_FIELD_KEY);
    }

    public static boolean isGetUsageStatsPermitted(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            try {
                PackageManager packageManager = context.getPackageManager();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);

                AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
                if (appOpsManager != null) {
                    appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
                }

                if (appOpsManager != null) {
                    return appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName) == AppOpsManager.MODE_ALLOWED;
                }
            } catch (Exception e) {
                e.printStackTrace();

                return false;
            }
        }

        return false;
    }

    public static boolean isServiceRunning(Context context, String serviceClassName){
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services;
        if (activityManager != null) {
            services = activityManager.getRunningServices(Integer.MAX_VALUE);

            for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
                if (runningServiceInfo.service.getClassName().equals(serviceClassName)){
                    return true;
                }
            }
        }

        return false;
    }

    public static void extractJSON2Intent(String JSON, Intent intent) throws JSONException {
        if (JSON.trim().length() == 0) {
            return;
        }

        JSONObject obj = new JSONObject(JSON);
        Iterator<String> iter = obj.keys();

        while (iter.hasNext()) {
            String key = iter.next();
            Object value = obj.get(key);

            if (value instanceof Boolean) {
                intent.putExtra(key, (boolean) value);
            } else if (value instanceof Integer) {
                intent.putExtra(key, (int) value);
            } else if (value instanceof Float) {
                intent.putExtra(key, (float) value);
            } else if (value instanceof  Double) {
                intent.putExtra(key, (double) value);
            } else if (value instanceof String) {
                intent.putExtra(key, (String) value);
            }
        }
    }
}
