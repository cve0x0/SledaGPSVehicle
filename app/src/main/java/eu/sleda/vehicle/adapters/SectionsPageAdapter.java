package eu.sleda.vehicle.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import eu.sleda.vehicle.interfaces.ActivityTab;

/**
 * Created by Cvetomir Marinov
 */

public class SectionsPageAdapter extends FragmentPagerAdapter {
    private List<ActivityTab> tabs = new ArrayList<>();

    public SectionsPageAdapter(FragmentManager fm, List<ActivityTab> tabs) {
        super(fm);
        this.tabs = tabs;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).getTitle();
    }
}
