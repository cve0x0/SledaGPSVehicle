package eu.sleda.vehicle.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.objects.FastMessage;

/**
 * Created by Cvetomir Marinov
 */

public class FastMessagesAdapter extends ArrayAdapter<FastMessage> {

    public FastMessagesAdapter(Context context, ArrayList<FastMessage> messages) {
        super(context, 0, messages);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fast_messages_item, parent, false);
        }

        FastMessage message = getItem(position);

        TextView fastMessageTextView = convertView.findViewById(R.id.fast_messages_item);
        if (message != null && fastMessageTextView != null) {
            fastMessageTextView.setText(message.getText());
        }

        return convertView;
    }
}
