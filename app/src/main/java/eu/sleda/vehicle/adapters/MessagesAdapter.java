package eu.sleda.vehicle.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.method.LinkMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.helpers.LocationParser;
import eu.sleda.vehicle.interfaces.OnLocationClick;
import eu.sleda.vehicle.interfaces.OnStatusChange;
import eu.sleda.vehicle.modules.ModuleLocation;
import eu.sleda.vehicle.objects.GEOLocation;
import eu.sleda.vehicle.objects.Message;

/**
 * Created by Cvetomir Marinov
 */

public class MessagesAdapter extends ArrayAdapter<Message> {
    private LocationParser locationParser = new LocationParser();

    public MessagesAdapter(Context context, ArrayList<Message> messages) {
        super(context, 0, messages);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Message msg = getItem(position);

        if (msg != null && msg.isViewAvailable()) {
            return msg.getView();
        }

        convertView = LayoutInflater.from(getContext()).inflate(R.layout.messages_item, parent, false);

        LinearLayout linearLayout = convertView.findViewById(R.id.messages_item_layout);

        TextView messageView = convertView.findViewById(R.id.message_item);

        String message = null;
        if (msg != null) {
            message = msg.getMessage();
        }

        if (message == null || message.isEmpty()) {
            message = getContext().getString(R.string.no_text);
        }

        messageView.setText(locationParser.parse(message, new OnLocationClick() {
            @Override
            public void onClick(ArrayList<GEOLocation> locations) {
                ModuleLocation moduleLocation = ModuleLocation.getInstance();
                moduleLocation.sygicNavigate(locations, getContext().getApplicationContext());
            }
        }));
        messageView.setMovementMethod(LinkMovementMethod.getInstance());

        ImageView imageImageView = convertView.findViewById(R.id.message_image);
        if (msg != null && msg.isFileImage()) {
            imageImageView.setVisibility(View.VISIBLE);
            imageImageView.setImageURI(Uri.fromFile(msg.getFile()));
        }

        final TextView dateView = convertView.findViewById(R.id.message_item_timestamp);
        final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:s", Locale.getDefault());
        String dateString = null;
        if (msg != null) {
            dateString = formatter.format(new Date(msg.getDate()));
        }

        if (msg != null) {
            if (msg.getType() == MessagesTypes.MESSAGE_RECEIVED) {
                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.message_received));
                if (msg.isSMS()) {
                    dateView.setText(getContext().getString(R.string.messages_item_received) + " " + dateString + " (SMS)");
                } else {
                    dateView.setText(getContext().getString(R.string.messages_item_received) + " " + dateString);
                }
                dateView.setGravity(Gravity.START);
                messageView.setGravity(Gravity.START);
            } else {
                if (msg.isSuccess()) {
                    if (msg.isSMS()) {
                        dateView.setText(getContext().getString(R.string.messages_item_sent) + " " + dateString + " (SMS)");
                    } else {
                        dateView.setText(getContext().getString(R.string.messages_item_sent) + " " + dateString);
                    }
                } else {
                    dateView.setText(getContext().getString(R.string.messages_item_waiting));

                    msg.setOnStatusChange(new OnStatusChange() {
                        @Override
                        public void onChange(boolean status) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    dateView.setText(getContext().getString(R.string.messages_item_sent) + " " + formatter.format(new Date(System.currentTimeMillis())));
                                }
                            });
                        }
                    });
                }

                linearLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.message_sent));
                dateView.setGravity(Gravity.END);
                messageView.setGravity(Gravity.END);
            }
        }

        if (msg != null) {
            msg.setView(convertView);
        }

        return convertView;
    }
}