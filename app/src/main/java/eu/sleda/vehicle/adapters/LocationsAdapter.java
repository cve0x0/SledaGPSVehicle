package eu.sleda.vehicle.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.objects.LocationAddress;

public class LocationsAdapter extends ArrayAdapter<LocationAddress> {
    public LocationsAdapter(Context context, ArrayList<LocationAddress> locationAddressArrayList) {
        super(context, 0, locationAddressArrayList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.locations_item, parent, false);
        }

        LocationAddress locationAddress = getItem(position);

        TextView locationTextView = convertView.findViewById(R.id.locations_item);
        if (locationAddress != null) {
            locationTextView.setText(locationAddress.getAddress());
        }

        return convertView;
    }
}
