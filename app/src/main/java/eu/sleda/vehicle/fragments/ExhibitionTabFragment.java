package eu.sleda.vehicle.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.configs.ConnectionTypes;
import eu.sleda.vehicle.interfaces.ActivityTab;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleNetworkChange;
import eu.sleda.vehicle.interfaces.ModulePhoneStateChange;
import eu.sleda.vehicle.interfaces.ModuleSpeed;
import eu.sleda.vehicle.interfaces.ModuleTemperature;
import eu.sleda.vehicle.listeners.StateListener;
import eu.sleda.vehicle.objects.Carrier;
import eu.sleda.vehicle.receivers.NetworkChangeReceiver;
import eu.sleda.vehicle.receivers.TemperatureReceiver;
import eu.sleda.vehicle.services.LocationTrackingService;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class ExhibitionTabFragment extends Fragment implements Module, ModulePhoneStateChange, ModuleNetworkChange, ModuleTemperature, ModuleSpeed, ActivityTab {
    private Watcher watcher;

    private View view;
    private StateListener phoneState;
    private NetworkChangeReceiver network;
    private LocationTrackingService locationTrackingService;

    public ExhibitionTabFragment() {
        watcher = Watcher.getInstance();
        phoneState = StateListener.getInstance();
        network = NetworkChangeReceiver.getInstance();
        locationTrackingService = LocationTrackingService.instance();

        watcher.modules.add("ExhibitionTabFragment", this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_exhibition, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        this.view = view;

        TextView usernameView = (TextView) view.findViewById(R.id.fragment_exhibition_details_box_username);
        usernameView.setText(getString(R.string.phone_number) + ": " + watcher.user.getPhone());

        TextView firstnameView = (TextView) view.findViewById(R.id.fragment_exhibition_details_box_firstname);
        firstnameView.setText(getString(R.string.fragment_exhibition_details_box_firstname) + ": " + watcher.user.getFirstname());

        TextView lastnameView = (TextView) view.findViewById(R.id.fragment_exhibition_details_box_lastname);
        lastnameView.setText(getString(R.string.fragment_exhibition_details_box_lastname) + ": " + watcher.user.getLastname());

        TextView companyView = (TextView) view.findViewById(R.id.fragment_exhibition_details_box_company);
        companyView.setText(getString(R.string.fragment_exhibition_details_box_company) + ": " + watcher.user.getCompany());

        onStateChange(phoneState.getState(), phoneState.getCarrier());
        onTemperatureChange(TemperatureReceiver.getInstance().getTemperature());
        onSpeedChange(locationTrackingService.hasSpeed() ? locationTrackingService.getSpeed() : -1);
    }

    @Override
    public String getTitle() {
        return watcher.getString(R.string.exhibiton_tab);
    }

    @Override
    public int getIcon() {
        return R.drawable.exhibition;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void onStateChange(int state, Carrier carrier) {
        if (view == null) {
            return;
        }

        TextView networkOperator = view.findViewById(R.id.fragment_exhibition_network_box_mobile_operator);

        String operator;

        switch (carrier.getIMSI()) {
            case -1:
                operator = getString(R.string.fragment_exhibition_not_available);
                break;

            case 0:
                operator = getString(R.string.fragment_exhibition_loading);
                break;

            default:
                operator = carrier.getName();
                break;
        }

        onNetworkChange(network.isNetworkAvailable(), network.getState(getContext()));

        networkOperator.setText(getString(R.string.fragment_exhibition_network_box_mobile_operator) + ": " + operator);
    }

    @Override
    public void onNetworkChange(boolean status, int state) {
        if (view == null) {
            return;
        }

        TextView connection = view.findViewById(R.id.fragment_exhibition_network_box_connection);

        String type = watcher.getString(R.string.fragment_exhibition_not_available);

        switch (Utils.getConnectionType()) {
            case ConnectionTypes.WIFI:
                type = "Wi-Fi";
                break;

            case ConnectionTypes.MOBILE:
                type = getString(R.string.fragment_exhibition_network_box_cellular_data);
                break;

            case ConnectionTypes.SMS:
                type = "SMS";
                break;
        }

        connection.setText(getString(R.string.fragment_exhibition_network_box_connection) + ": " + type);
    }

    @Override
    public void onSpeedChange(int speed) {
        if (view == null) {
            return;
        }

        TextView speedView = view.findViewById(R.id.fragment_exhibition_info_box_current_speed);

        if (speedView == null) {
            return;
        }

        String speedText;

        if (speed == -1) {
            speedText = getString(R.string.fragment_exhibition_not_available);
        } else {
            speedText = speed + " km/h";
        }

        speedView.setText(getString(R.string.fragment_exhibition_info_box_current_speed) + ": " + speedText);
    }

    @Override
    public void onTemperatureChange(byte temperature) {
        if (view == null) {
            return;
        }

        TextView temperatureView = view.findViewById(R.id.fragment_exhibition_info_box_temperature);
        temperatureView.setText(watcher.getString(R.string.fragment_exhibition_info_box_temperature) + ": " + temperature + " °C");
    }
}