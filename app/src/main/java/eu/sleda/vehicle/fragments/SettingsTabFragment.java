package eu.sleda.vehicle.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.preference.PreferenceFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.activities.LockActivity;
import eu.sleda.vehicle.helpers.Alert;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.ActivityTab;
import eu.sleda.vehicle.services.AppLockService;
import eu.sleda.vehicle.utils.Utils;

public class SettingsTabFragment extends PreferenceFragment implements ActivityTab, SharedPreferences.OnSharedPreferenceChangeListener {
    SharedPreferences sharedPreferences;

    private Preference setupAppLockPreference;

    private static class ACTIONS {
        static final int
                USAGE_SETTINGS = 1000,
                MANAGE_OVERLAY = 1001,
                APP_LOCK = 1002;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getContext() != null) {
            try {
                sharedPreferences = getContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (sharedPreferences != null) {
            sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        }

        addPreferencesFromResource(R.xml.settings);

        findPreference("pref_imei").setSummary(Utils.getIMEI(getContext()));
        findPreference("pref_version").setSummary(getContext().getString(R.string.fragment_settings_current_version_stable) + " " + Utils.getVersion(getContext()));

        findPreference("delete_messages").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                removeMessages();
                return false;
            }
        });

        this.setupAppLockPreference = findPreference("setup_applock");

        setupAppLockPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                setupUnsetupAppLockAction();

                return false;
            }
        });

        setupUnsetupAppLockAction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (sharedPreferences != null) {
            sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIONS.APP_LOCK:
                if (resultCode == Activity.RESULT_OK) {
                    setupUnsetupAppLockAction();
                }

            case ACTIONS.USAGE_SETTINGS:
            case ACTIONS.MANAGE_OVERLAY:
                this.setupAppLockClick();

                break;
        }
    }

    private void setupAppLockClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!Utils.isGetUsageStatsPermitted(getContext())) {
                startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS), ACTIONS.USAGE_SETTINGS);
                return;
            }
        }

        if (getContext() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(getContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getContext().getPackageName()));
                    startActivityForResult(intent, ACTIONS.MANAGE_OVERLAY);
                    return;
                }
            }
        }

        if (Utils.isAppLockerActivated(getContext()) && !Utils.isServiceRunning(getContext(), AppLockService.class.toString())) {
            if (getContext() != null){
                try {
                    getContext().startService(new Intent(getContext(), AppLockService.class));
                } catch (Exception e) {
                    Logger.e(e.getMessage());
                    Logger.e(e.toString());

                    e.printStackTrace();
                }
            }
        } else {
            startActivityForResult(new Intent(getContext(), LockActivity.class), ACTIONS.APP_LOCK);
        }
    }

    private void setupUnsetupAppLockAction() {
        if (Utils.isAppLockerActivated(getContext())) {
            getPreferenceScreen().removePreference(setupAppLockPreference);
        } else {
            getPreferenceScreen().addPreference(setupAppLockPreference);
            setupAppLockPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    setupAppLockClick();

                    return false;
                }
            });
        }
    }

    public void removeMessages() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    DispatcherTabFragment dispatcher = DispatcherTabFragment.instance();
                    dispatcher.messagesDatabase.clearMessages();
                    dispatcher.messages.clear();
                    dispatcher.adapter.notifyDataSetChanged();
                    dispatcher.emptyIcon.setVisibility(View.VISIBLE);

                    new Alert(getContext()).show(getString(R.string.settings_alert_delete_messages_success));
                }
            }
        };

        if (getContext() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getString(R.string.confirm_question)).setMessage(getString(R.string.settings_alert_delete_messages_message)).setPositiveButton(getString(R.string.positive), dialogClickListener).setNegativeButton(getString(R.string.negative), dialogClickListener).show();
        }
    }

    @Override
    public String getTitle() {
        return Watcher.getContext().getString(R.string.settings_tab);
    }

    @Override
    public int getIcon() {
        return R.drawable.settings;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        setupUnsetupAppLockAction();
    }
}
