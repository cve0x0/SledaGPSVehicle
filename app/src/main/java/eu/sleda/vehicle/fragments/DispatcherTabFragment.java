package eu.sleda.vehicle.fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.activities.FastMessagesActivity;
import eu.sleda.vehicle.activities.MessageActivity;
import eu.sleda.vehicle.adapters.MessagesAdapter;
import eu.sleda.vehicle.databases.MessagesDatabase;
import eu.sleda.vehicle.helpers.Settings;
import eu.sleda.vehicle.interfaces.ActivityTab;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.Message;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class DispatcherTabFragment extends Fragment implements ActivityTab {

    private static DispatcherTabFragment instance;

    private Watcher watcher = Watcher.getInstance();
    private Settings settings = watcher.getSettings();
    public MessagesDatabase messagesDatabase = watcher.getMessagesDatabase();
    public ArrayList<Message> messages = new ArrayList<>();
    public MessagesAdapter adapter = new MessagesAdapter(watcher, messages);
    public View emptyIcon;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dispatcher, container, false);
    }

    public static DispatcherTabFragment instance() {
        return instance;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        instance = this;

        ListView messagesListView = (ListView) view.findViewById(R.id.fragment_dispatcher_list_view);
        messagesListView.setAdapter(adapter);

        Button clickButton = (Button) view.findViewById(R.id.fragment_dispatcher_send_message);
        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), FastMessagesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        for (Message message : messagesDatabase.getMessagesHistory(168)) {
            messages.add(0, message);
            adapter.notifyDataSetChanged();

            if (!message.isSuccess()) {
                ModuleMessages.instance().putQueueMessage(message);
            }
        }

        emptyIcon = view.findViewById(R.id.fragment_dispatcher_no_messages_layout);

        hideEmptyIcon();
    }

    private void hideEmptyIcon() {
        if (messages.size() > 0 && emptyIcon.getVisibility() == View.VISIBLE) {
            emptyIcon.setVisibility(View.INVISIBLE);
        }
    }

    public void onMessageAdd(final Message message) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onMessageAdd(message);
                }
            });

            return;
        }

        if (settings.getBool(Settings.SAVE_MESSAGES)) {
            messagesDatabase.writeMessage(message);
        }

        messages.add(0, message);

        adapter.notifyDataSetChanged();

        hideEmptyIcon();
    }

    public void onMessageReceive(final Message message) {
        onMessageAdd(message);

        Intent intent = new Intent("sledagpsvehicle.action.message");
        intent.setComponent(new ComponentName(watcher.getPackageName(), MessageActivity.class.getName()));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("message", message.getMessage());

        watcher.startActivity(intent);
    }

    @Override
    public String getTitle() {
        return watcher.getString(R.string.dispatcher_tab);
    }

    @Override
    public int getIcon() {
        return R.drawable.dispather;
    }

    @Override
    public Fragment getFragment() {
        return this;
    }
}
