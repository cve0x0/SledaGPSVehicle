package eu.sleda.vehicle.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.HashMap;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Alert;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.Updater;
import eu.sleda.vehicle.interfaces.OnLogin;
import eu.sleda.vehicle.listeners.StateListener;
import eu.sleda.vehicle.modules.ModuleLogin;
import eu.sleda.vehicle.receivers.NetworkChangeReceiver;
import eu.sleda.vehicle.services.AppLockService;
import eu.sleda.vehicle.supplicants.RuntimePermissions;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class LaunchActivity extends AppCompatActivity {
    private static Context context;
    private Alert alert;
    private Updater updater;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        context = this;

        alert = new Alert(context);
        updater = new Updater(this);

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (telephonyManager != null) {
            telephonyManager.listen(new StateListener(), PhoneStateListener.LISTEN_SERVICE_STATE);
        }

        getApplicationContext().registerReceiver(
                new NetworkChangeReceiver(getApplicationContext()),
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        );

        if (Utils.isAppLockerActivated(context) && !Utils.isServiceRunning(context, AppLockService.class.toString())) {
            try {
                context.startService(new Intent(context, AppLockService.class));
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!RuntimePermissions.checkPermissions(this)) {
                Logger.w("Runtime permissions not granted");

                RuntimePermissions.requestPermissions(this);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (alert != null) {
            alert.stopProgress();
        }
    }

    private OnLogin onLogin = new OnLogin() {
        @Override
        public void onSuccess(HashMap<String, Object> details) {
            Intent intent = new Intent(getApplicationContext(), Watcher.class);
            intent.putExtra("userID", (int) details.get("userID"));
            intent.putExtra("token", (String) details.get("token"));
            intent.putExtra("phone", (String) details.get("phone"));

            startService(intent);
        }

        @Override
        public void onUpdate(int version) {
            if (alert != null) {
                alert.stopProgress();
            }

            if (updater != null) {
                updater.update(version);
            }
        }

        @Override
        public void onInvalidDetails() {
            if (alert != null) {
                alert.show(getString(R.string.launchactivity_login_details_error));
            }
        }

        @Override
        public void onNetworkNotAvailable() {
            if (alert != null) {
                alert.show(getString(R.string.launchactivity_alert_no_internet_connection));
            }
        }

        @Override
        public void onConnectionError() {
            if (alert != null) {
                alert.show(getString(R.string.launchactivity_alert_connection_error));
            }
        }
    };

    public void onLogin(View v) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!RuntimePermissions.checkPermissions(this)) {
                Logger.w("Runtime permissions not granted");

                RuntimePermissions.requestPermissions(this);

                return;
            }
        }

        EditText phoneView = (EditText) findViewById(R.id.activity_launch_phone);
        EditText pinView = (EditText) findViewById(R.id.activity_launch_pin);

        String phone = phoneView.getText().toString(), PIN = pinView.getText().toString();

        if (phone.isEmpty() || PIN.isEmpty()) {
            if (alert != null) {
                alert.show(getString(R.string.launchactivity_alert_empty));
            }

            return;
        }

        if (phone.length() < 8 || phone.length() > 15 || PIN.length() < 4 || PIN.length() > 6) {
            if (alert != null) {
                alert.show(getString(R.string.launchactivity_alert_invalid));
            }

            return;
        }

        if (alert != null) {
            alert.startProgress(getString(R.string.launchactivity_progress_text));
        }

        new Thread(new ModuleLogin(onLogin, updater, phone, PIN)).start();
    }
}
