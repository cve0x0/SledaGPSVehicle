package eu.sleda.vehicle.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.helpers.Logger;

/**
 * Created by Cvetomir Marinov
 */

public class UnlockDialog extends Dialog {
    private Context context;
    private SharedPreferences sharedPreferences;
    private Animation shakeAnimation;
    private EditText lockUnlockEditText;
    private TextView errorMessageTextView;

    public UnlockDialog(@NonNull Context context) {
        super(context, R.style.AppTheme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_lock);

        context = getContext();
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        setCanceledOnTouchOutside(false);
        setCancelable(false);

        Window dialogWindow = getWindow();
        if (dialogWindow == null)
            return;

        dialogWindow.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        dialogWindow.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialogWindow.setGravity(Gravity.CENTER);

        errorMessageTextView = findViewById(R.id.activity_lock_error_message);
        shakeAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.shake);

        lockUnlockEditText = findViewById(R.id.activity_lock_lock_unlock_code);
        Button lockUnlockButton = findViewById(R.id.activity_lock_lock_unlock);

        lockUnlockEditText.setHint(context.getString(R.string.applock_unlock_code));
        lockUnlockButton.setText(context.getString(R.string.applock_unlock));

        lockUnlockButton.setOnClickListener(this.onUnlockBtnClick);

        setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                    startMain.addCategory(Intent.CATEGORY_HOME);
                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(startMain);

                    return true;
                }

                return false;
            }
        });

    }

    private View.OnClickListener onUnlockBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String PIN = lockUnlockEditText.getText().toString();

            byte[] lockUnlockCodeBase64Decoded;

            try {
                lockUnlockCodeBase64Decoded = Base64.decode(sharedPreferences.getString(Config.PREFERENCE_CODE_FIELD_KEY, null), Base64.DEFAULT);
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();

                displayErrorMessage(context.getString(R.string.applock_invalid_code));

                return;
            }

            if (lockUnlockCodeBase64Decoded == null) {
                displayErrorMessage(context.getString(R.string.applock_invalid_code));

                return;
            } else {
                if (PIN.length() < 4 || PIN.length() > 8) {
                    displayErrorMessage(context.getString(R.string.applock_invalid_code));

                    return;
                }

                if (!PIN.equals(new String(lockUnlockCodeBase64Decoded))) {
                    displayErrorMessage(context.getString(R.string.applock_wrong_code));

                    return;
                }
            }

            dismiss();
        }
    };

    private void displayErrorMessage(String message) {
        errorMessageTextView.setText(message);
        errorMessageTextView.setAnimation(shakeAnimation);
    }
}
