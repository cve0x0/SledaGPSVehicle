package eu.sleda.vehicle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONException;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.adapters.FastMessagesAdapter;
import eu.sleda.vehicle.databases.MessagesDatabase;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.FastMessage;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class FastMessagesActivity extends AppCompatActivity {
    private Watcher watcher = Watcher.getInstance();
    private ModuleMessages moduleMessages = ModuleMessages.instance();
    private ArrayList<FastMessage> messages = new ArrayList<>();
    private MessagesDatabase messagesDatabase = null;
    private FastMessagesAdapter adapter = new FastMessagesAdapter(watcher, messages);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_messages);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            messagesDatabase = watcher.getMessagesDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }

        messagesDatabase.getFastMessages(messages);

        final ListView messagesListView = findViewById(R.id.activity_fast_messages_list_view);

        if (messagesListView != null) {

            messagesListView.setAdapter(adapter);
            messagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FastMessage message = messages.get((int) id);

                    if (message.haveAction()) {
                        try {
                            String activities_package = "eu.sleda.vehicle.activities";
                            String action = message.getAction().trim();

                            switch (action.toLowerCase()) {
                                case "keyboardmessage":
                                    action = activities_package + ".KeyboardMessageActivity";
                                    break;

                                case "voicemessage":
                                    action = activities_package + ".VoiceMessageActivity";
                                    break;

                                case "locations":
                                    action = activities_package + ".LocationsActivity";
                                    break;
                            }

                            Intent intent = new Intent(getApplicationContext(), Class.forName(action));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            Utils.extractJSON2Intent(message.getData(), intent);
                            startActivity(intent);
                        } catch (ClassNotFoundException | JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        moduleMessages.writeMessage(message.toMessage());
                    }

                    finish();
                }
            });
        }
    }
}
