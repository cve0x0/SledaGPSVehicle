package eu.sleda.vehicle.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;
import java.util.Vector;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.adapters.SectionsPageAdapter;
import eu.sleda.vehicle.fragments.DispatcherTabFragment;
import eu.sleda.vehicle.fragments.ExhibitionTabFragment;
import eu.sleda.vehicle.fragments.SettingsTabFragment;
import eu.sleda.vehicle.helpers.Alert;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.ActivityTab;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class MainActivity extends AppCompatActivity {
    private Watcher watcher = Watcher.getInstance();
    private List<ActivityTab> tabs = new Vector<>();
    private static Context context;
    private Alert alert;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Logger.d("MainActivity::onCreate()");

        context = this;

        alert = new Alert(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabs.add(new ExhibitionTabFragment());
        tabs.add(new DispatcherTabFragment());
        tabs.add(new SettingsTabFragment());

        SectionsPageAdapter mSectionsPagerAdapter = new SectionsPageAdapter(getSupportFragmentManager(), tabs);

        ViewPager mViewPager = findViewById(R.id.activity_main_view_pager);

        if (mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);

            mViewPager.setOffscreenPageLimit(tabs.size());

            TabLayout tabLayout = findViewById(R.id.activity_main_tabs);

            if (tabLayout != null) {
                tabLayout.setupWithViewPager(mViewPager);

                for (int i = 0; i < tabs.size(); ++i) {
                    if (tabLayout.getTabAt(i) != null) {
                        tabLayout.getTabAt(i).setIcon(tabs.get(i).getIcon());
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            alert.startProgress("Logout...");
            watcher.getHandler().sendEmptyMessage(1);
        }

        return id == R.id.action_logout || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        alert.stopProgress();
    }
}
