package eu.sleda.vehicle.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.helpers.AudioRecorder;
import eu.sleda.vehicle.interfaces.OnTimeUpdate;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.Message;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class VoiceMessageActivity extends AppCompatActivity {
    private AudioRecorder recorder;
    private TextView timeTextView;
    private Button sendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_message);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        timeTextView = findViewById(R.id.activity_voice_message_counter);
        sendButton = findViewById(R.id.activity_voice_message_send);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // If recording active, stop it and delete file
        if (recorder != null && recorder.isRecording()) {
            recorder.stop();
            recorder.getFile().delete();
        }
    }

    public void onVoiceRecordClick(final View view) {
        if(recorder != null && recorder.isRecording()) {
            recorder.stop();
            view.setBackground(ContextCompat.getDrawable(this, R.drawable.start_recording_button_background));
            sendButton.setVisibility(View.VISIBLE);
        } else {
            sendButton.setVisibility(View.GONE);
            recorder = new AudioRecorder(Utils.createAppDirectory(Config.TMP_DIRECTORY) + System.currentTimeMillis() + ".mp3");
            view.setBackground(ContextCompat.getDrawable(this, R.drawable.stop_recording_button_background));

            recorder.setTimer(new OnTimeUpdate() {
                @Override
                public void onTimeUpdate(byte minutes, byte seconds) {
                    timeTextView.setText(String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                    if (minutes == 1) onVoiceRecordClick(view);
                }
            });

            recorder.start();
        }
    }

    public void onSendVoiceClick(View view) {
        Message message = new Message("");
        message.setFile(recorder.getFile(), MessagesTypes.MESSAGE_VOICE);
        ModuleMessages.instance().writeMessage(message);
        finish();
    }
}