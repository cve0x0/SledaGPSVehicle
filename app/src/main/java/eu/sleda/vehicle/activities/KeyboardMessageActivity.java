package eu.sleda.vehicle.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.Message;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class KeyboardMessageActivity extends AppCompatActivity {
    private ModuleMessages moduleMessages = ModuleMessages.instance();
    private ImageView imageView;
    private File file;
    private boolean isStandart;
    private Uri imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard_message);

        Intent intent = getIntent();

        if (intent.hasExtra("text") && !intent.getBooleanExtra("text", false)) {
            findViewById(R.id.activity_keyboard_text_message).setVisibility(View.GONE);
        }

        if (intent.hasExtra("image") && !intent.getBooleanExtra("image", false)) {
            findViewById(R.id.activity_keyboard_image_message).setVisibility(View.GONE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageView = (ImageView) findViewById(R.id.activity_keyboard_image);

        Button addLowImageButton = (Button) findViewById(R.id.activity_keyboard_add_photo_low_quality);
        addLowImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStandart = false;
                openCameraActivity();
            }
        });

        Button addNormalImageButton = (Button) findViewById(R.id.activity_keyboard_add_photo_normal_quality);
        addNormalImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStandart = true;
                openCameraActivity();
            }
        });
    }

    private void openCameraActivity() {

        File file = new File(Utils.createAppDirectory(Config.TMP_DIRECTORY), System.currentTimeMillis() + ".jpeg");

        Uri cameraIntentUri = null;

        try {
            cameraIntentUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", file);
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }

        if (cameraIntentUri != null) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraIntentUri);

            imagePath = Uri.fromFile(file);

            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, cameraIntentUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }

            startActivityForResult(cameraIntent, 1);
        }
    }

    public void onSendMessageClick(View view) {
        EditText messageEditText = findViewById(R.id.activity_keyboard_message_text);
        String msg = messageEditText.getText().toString();

        Message message = new Message(msg);

        if (file != null) {
            message.setFile(file, MessagesTypes.MESSAGE_IMAGE);
        }

        moduleMessages.writeMessage(message);

        finish();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (imagePath != null) {
                getContentResolver().notifyChange(imagePath, null);

                Bitmap bitmap = getBitmap(imagePath.getPath());
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    private Bitmap getResizedBitmap(Bitmap bitmap, int maxWidth, int maxHeight) {
        try {
            float scale = Math.min(((float) maxHeight / bitmap.getWidth()), ((float) maxWidth / bitmap.getHeight()));
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();

            return resizedBitmap;
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }

        return null;
    }

    private Bitmap getBitmap(String path) {
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path);

            if (isStandart) {
                bitmap = getResizedBitmap(bitmap, 850, 850);
            } else {
                bitmap = getResizedBitmap(bitmap, 310, 310);
            }

            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, isStandart ? 100 : 80, new FileOutputStream(path));
            }

            file = new File(path);

            return bitmap;
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }

        return null;
    }
}