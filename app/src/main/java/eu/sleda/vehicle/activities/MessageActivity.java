package eu.sleda.vehicle.activities;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.LocationParser;
import eu.sleda.vehicle.helpers.Settings;
import eu.sleda.vehicle.interfaces.OnLocationClick;
import eu.sleda.vehicle.modules.ModuleLocation;
import eu.sleda.vehicle.objects.GEOLocation;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class MessageActivity extends AppCompatActivity {
    private Watcher watcher = Watcher.getInstance();
    private Settings settings = watcher.getSettings();
    private Vibrator vibrator;
    private boolean flag = true;

    private Thread notificationThread = new Thread() {
        public void run() {
            boolean sound = settings.getBool(Settings.MESSAGE_SOUND);
            boolean vibrate = settings.getBool(Settings.MESSAGE_VIBRATE);

            while (flag && (sound || vibrate)) {
                if (sound) MediaPlayer.create(Watcher.getContext(), R.raw.message).start();
                if (vibrate) vibrator.vibrate(2 * 1000);

                try {
                    Thread.sleep(4 * 1000);
                } catch (InterruptedException ignore) {

                }
            }
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle parms = getIntent().getExtras();

        String message = null;
        if (parms != null) {
            message = parms.getString("message");
        }

        if (message == null || message.isEmpty()) {
            message = getString(R.string.no_text);
        }

        TextView messageView = findViewById(R.id.activity_message_text);
        messageView.setText(new LocationParser().parse(message, new OnLocationClick() {
            @Override
            public void onClick(ArrayList<GEOLocation> locations) {
                ModuleLocation.getInstance().sygicNavigate(locations, getApplicationContext());
                finish();
            }
        }));
        messageView.setMovementMethod(LinkMovementMethod.getInstance());

        notificationThread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        vibrator.cancel();
        flag = false;
    }

    public void openFastMessages(View v) {
        Intent intent = new Intent(this, FastMessagesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
