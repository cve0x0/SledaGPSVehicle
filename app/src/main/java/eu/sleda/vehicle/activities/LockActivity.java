package eu.sleda.vehicle.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.helpers.Logger;

public class LockActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private Animation shakeAnimation;
    private EditText lockUnlockEditText;
    private TextView errorMessageTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);

        this.sharedPreferences = getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
        this.shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        this.lockUnlockEditText = findViewById(R.id.activity_lock_lock_unlock_code);
        this.errorMessageTextView = findViewById(R.id.activity_lock_error_message);

        Button lockUnlockButton = findViewById(R.id.activity_lock_lock_unlock);

        if (lockUnlockButton != null) {
            lockUnlockEditText.setHint(getString(R.string.applock_lock_code));
            lockUnlockButton.setText(getString(R.string.applock_lock));
            lockUnlockButton.setOnClickListener(onUnlockBtnClick);
        }
    }

    private View.OnClickListener onUnlockBtnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String PIN = lockUnlockEditText.getText().toString();

            if (PIN.isEmpty() || PIN.length() < 4 || PIN.length() > 8) {
                displayErrorMessage(getString(R.string.applock_invalid_code));

                return;
            }

            byte[] lockUnlockCodeBase64Encoded;

            try {
                lockUnlockCodeBase64Encoded = Base64.encode(PIN.getBytes(), Base64.DEFAULT);
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();

                displayErrorMessage(getString(R.string.applock_invalid_code));

                return;
            }

            if (lockUnlockCodeBase64Encoded == null) {
                displayErrorMessage(getString(R.string.applock_invalid_code));
            } else {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Config.PREFERENCE_CODE_FIELD_KEY, new String(lockUnlockCodeBase64Encoded));
                editor.apply();

                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);

                finishActivity();
            }
        }
    };

    private void displayErrorMessage(String message) {
        errorMessageTextView.setText(message);
        errorMessageTextView.setAnimation(shakeAnimation);
    }

    private void finishActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        } else {
            finish();
        }
    }
}
