package eu.sleda.vehicle.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.adapters.LocationsAdapter;
import eu.sleda.vehicle.databases.MessagesDatabase;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.LocationAddress;
import eu.sleda.vehicle.objects.Message;

public class LocationsActivity extends AppCompatActivity {
    private Watcher watcher = Watcher.getInstance();
    private ArrayList<LocationAddress> locationAddresses = new ArrayList<>();
    private LocationsAdapter adapter = new LocationsAdapter(watcher, locationAddresses);
    private MessagesDatabase messagesDatabase = watcher.getMessagesDatabase();
    public View emptyIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        messagesDatabase.getAddressLocations(locationAddresses);

        final ListView locationsListView = findViewById(R.id.activity_locations_list_view);

        emptyIcon = findViewById(R.id.activity_locations_no_data_layout);

        if (locationAddresses.isEmpty()) {
            locationsListView.setVisibility(View.INVISIBLE);

            emptyIcon.setVisibility(View.VISIBLE);
        } else {
            emptyIcon.setVisibility(View.INVISIBLE);

            locationsListView.setVisibility(View.VISIBLE);

            locationsListView.setAdapter(adapter);

            locationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        LocationAddress locationAddress = locationAddresses.get(position);
                        locationAddresses.remove(locationAddress);

                        messagesDatabase.removeAddressLocation(locationAddress);

                        ModuleMessages.instance().writeMessage(new Message("Visited [l:" + locationAddress.getLatitude() + ";" + locationAddress.getLongitude() + "]"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    finish();
                }
            });
        }
    }
}
