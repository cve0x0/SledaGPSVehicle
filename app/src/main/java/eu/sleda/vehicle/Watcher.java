package eu.sleda.vehicle;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;

import java.util.ArrayList;

import eu.sleda.vehicle.activities.LaunchActivity;
import eu.sleda.vehicle.activities.MainActivity;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.Protocol;
import eu.sleda.vehicle.databases.MessagesDatabase;
import eu.sleda.vehicle.helpers.IntentService;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.Modules;
import eu.sleda.vehicle.helpers.PackageBuffer;
import eu.sleda.vehicle.helpers.Settings;
import eu.sleda.vehicle.helpers.Timer;
import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleLogout;
import eu.sleda.vehicle.interfaces.ModuleRegularlyRequest;
import eu.sleda.vehicle.interfaces.ModuleRequest;
import eu.sleda.vehicle.interfaces.WakeUp;
import eu.sleda.vehicle.modules.ModuleDetails;
import eu.sleda.vehicle.modules.ModuleLocation;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.modules.ModuleServices;
import eu.sleda.vehicle.objects.User;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class Watcher extends IntentService {

    public User user;
    private EasyTCPClient conn;
    public Modules modules = new Modules();
    private Settings settings;
    private MessagesDatabase messagesDatabase;
    private static Context context;
    private int fastMessagesVersion;
    private Timer regularlyTimer = new Timer(Config.REGULARLY_INTERVAL * 1000), timeoutTimer = new Timer(Config.CONNECTION_TIMEOUT_INTERVAL * 1000);

    public Watcher() {
        super("Watcher");
        Logger.d("Watcher() -> Create instance");
    }

    public static Context getContext() {
        return context;
    }

    public static Watcher getInstance() {
        return (Watcher) getContext();
    }

    public EasyTCPClient getConnection() {
        return conn;
    }

    private boolean renewConnection(EasyTCPClient conn) {
        Logger.d("OnWakeUp::renewConnection()");

        conn.close();
        conn.connect(Config.SERVER, Config.PORT, Config.SOCKET_CONNECT_TIMEOUT * 1000);

        if (!conn.isConnected()) {
            return false;
        }

        conn.setTimeout(Config.SOCKET_READ_TIMEOUT * 1000);
        conn.streamFlush(false);

        PackageBuffer buf = new PackageBuffer();

        buf.writeByte(Protocol.LOGIN_TOKEN);
        buf.writeInt(user.getUserId());
        buf.writeChunkString(user.getToken());
        conn.writeBytes(buf.toByteArray());
        conn.flush();

        byte response = conn.readByte();

        if (response == Protocol.LOGIN_TOKEN_INVALID) {
            stopSelf();

            return false;
        }

        Logger.d("OnWakeUp::renewConnection() <- " + (response == Protocol.LOGIN_TOKEN_SUCCESS));

        if (response == Protocol.LOGIN_TOKEN_SUCCESS) {
            user.setFirstname(conn.readChunkString());
            user.setLastname(conn.readChunkString());
            user.setCompany(conn.readChunkString());
            updateFastMessages(conn.readByte());
            user.setInterval(conn.readShort());

            return true;
        } else {
            conn.close();
            return false;
        }
    }

    public boolean connected() {
        if (!conn.isConnected() && Utils.isConnectionReady() && renewConnection(conn))
            timeoutTimer.reset();

        return conn.isConnected();
    }

    public void onFastMessagesUpdate() {
        messagesDatabase.clearFastMessages();

        int i = conn.readUnsignedByte();
        if (conn.err) {
            logout();
            return;
        }

        while (i-- > 0) {
            String message = conn.readChunkString(500);
            if (message == null || message.length() == 0) {
                logout();
                return;
            }

            messagesDatabase.insertFastMessage(message);
        }

        settings.put(Settings.FAST_MESSAGES_VERSION, fastMessagesVersion);
    }

    private Runnable bufferWatchRunnable = new Runnable() {
        @Override
        public void run() {
            if (connected()) {
                while (conn.available() > 0) {
                    byte INS = conn.readByte();

                    switch (INS) {
                        case Protocol.RECEIVE_MESSAGES:
                            ModuleMessages.instance().readMessages();
                            break;

                        case Protocol.RECEIVE_PACKAGE_SUCCESS:
                            byte packageID = conn.readByte();
                            if (!conn.err)
                                wakeUpRunnable.onSuccessReceive(packageID);
                            break;

                        case Protocol.UPDATE_FAST_MESSAGES:
                            onFastMessagesUpdate();
                            break;

                        case Protocol.NULL:
                            conn.writeByte(Protocol.NULL);
                            timeoutTimer.reset();
                            break;
                    }
                }

                if (regularlyTimer.isReady()) {
                    wakeUpRunnable.onRequest(true);
                    regularlyTimer.reset();
                }

                if (timeoutTimer.isReady() && conn.isConnected())
                    conn.close();
            }

            getHandler().postDelayed(this, 3 * 1000);
        }
    };

    private WakeUp wakeUpRunnable = new WakeUp() {
        private SparseArray<SparseArray<Object>> options = new SparseArray();
        private byte packageID;

        @Override
        public void run() {
            if (connected())
                onRequest(false);

            getHandler().postDelayed(this, user.getInterval() * 1000);
        }

        @Override
        public void onRequest(boolean regularly) {
            PackageBuffer buf = new PackageBuffer();

            for (Module module : modules.getModules()) {
                if (!regularly && module instanceof ModuleRequest) {
                    ((ModuleRequest) module).onRequest(buf);
                } else if (regularly && module instanceof ModuleRegularlyRequest) {
                    ((ModuleRegularlyRequest) module).onRegularlyRequest(buf);
                }
            }

            if (buf.size() > 0) {
                if (buf.requireResponse) {
                    buf.writeByte(Protocol.SEND_PACKAGE);
                    buf.writeByte(packageID);
                }

                if (conn.writeBytes(buf.toByteArray()) && conn.flush() && buf.requireResponse)
                    options.append(packageID++, buf.options);
            }
        }

        @Override
        public void onSuccessReceive(byte packageID) {
            SparseArray<Object> opt = options.get(packageID);

            if (opt != null) {
                for (Module module : modules.getModules()) {
                    if (module instanceof ModuleRequest) {
                        ((ModuleRequest) module).onRequestSuccess(opt);
                    }
                }

                options.remove(packageID);
            }
        }
    };

    @Override
    public void onMessage(Message message) {
        logout();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Logger.d("Watcher::onHandleIntent()");
        Bundle parms = intent.getExtras();

        context = this;

        int userID = parms.getInt("userID");
        String token = parms.getString("token");
        String phone = parms.getString("phone");

        conn = new EasyTCPClient();
        user = new User(userID, phone, token);
        settings = new Settings(this);
        messagesDatabase = new MessagesDatabase(this);

        if (!renewConnection(conn))
            return;

        Logger.d("Watcher::onHandleIntent() -> Phone: " + user.getPhone());

        modules.add("messages", new ModuleMessages());
        modules.add("services", new ModuleServices());

        this.removeOldMessages();

        Intent i = new Intent(getBaseContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getApplication().startActivity(i);

        Utils.sleep(3 * 1000);

        modules.add("location", new ModuleLocation());
        modules.add("details", new ModuleDetails());

        regularlyTimer.reset();
        timeoutTimer.reset();
        wakeUpRunnable.run();
        bufferWatchRunnable.run();
    }

    private void removeOldMessages() {
        ArrayList<eu.sleda.vehicle.objects.Message> messages = messagesDatabase.getMessagesBefore(168);

        for (eu.sleda.vehicle.objects.Message message: messages) {
            Logger.d("Message for delete: " + message.getMessage());

            if (message.isFileAvailable()) {
                Logger.d("Delete file: " + message.getFile().getAbsolutePath());
                message.removeFile();
            }

            messagesDatabase.removeMessage(message);
        }
    }

    @Override
    public void onTaskRemoved(final Intent rootIntent) {
        logout(true);
    }

    public MessagesDatabase getMessagesDatabase() {
        return messagesDatabase;
    }

    public Settings getSettings() {
        return settings;
    }

    public void updateFastMessages(byte version) {
        int currentVersion = settings.getInt(Settings.FAST_MESSAGES_VERSION);
        Logger.d("Curretn fast messages version : " + currentVersion + " | Version: " + version);
        if (version > currentVersion || (version == 0 && version != currentVersion)) {
            Logger.d("Watcher::updateFastMessages() -> " + currentVersion + " to " + version);
            fastMessagesVersion = version;
            conn.writeByte(Protocol.UPDATE_FAST_MESSAGES);
            conn.flush();
        }
    }

    @Override
    public void onDestroy() {
        context = null;
        super.onDestroy();
    }

    public void logout() {
        logout(false);
    }

    public void logout(boolean isOnTask) {
        Logger.d("Watcher::logout()");

        for (Module m : modules.getModules()) {
            if (m instanceof ModuleLogout) {
                ((ModuleLogout) m).onLogout();
            }
        }

        if (Looper.myLooper() == getHandler().getLooper()) {
            conn.writeByte(Protocol.LOGOUT);
            conn.close();

            if (!isOnTask) {
                Intent intent = new Intent(this, LaunchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }

        stopSelf();
    }
}