package eu.sleda.vehicle.helpers;

import android.util.Log;

import eu.sleda.vehicle.configs.Config;

/**
 * Created by Cvetomir Marinov
 */

public class Logger {

    public static void v(String message) {
        if (Config.VERBOSE_LOG) Log.v(Config.TAG, message);
    }

    public static void d(String message) {
        if (Config.DEBUG_LOG) Log.d(Config.TAG, message);
    }

    public static void i(String message) {
        if (Config.INFO_LOG) Log.i(Config.TAG, message);
    }

    public static void w(String message) {
        if (Config.WARN_LOG) Log.w(Config.TAG, message);
    }

    public static void e(String message) {
        if (Config.ERROR_LOG) Log.e(Config.TAG, message);
    }

}
