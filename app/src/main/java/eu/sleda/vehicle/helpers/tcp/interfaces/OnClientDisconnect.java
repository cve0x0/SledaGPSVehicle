package eu.sleda.vehicle.helpers.tcp.interfaces;

import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;

/**
 * Created by Cvetomir Marinov
 */

public interface OnClientDisconnect {
    void onDisconnect(EasyTCPClient conn);
}
