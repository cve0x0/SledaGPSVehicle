package eu.sleda.vehicle.helpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;

/**
 * Created by Cvetomir Marinov
 */

public class Alert {
    private ProgressDialog dialog;
    private boolean isProgressWorking = false;
    private AlertDialog.Builder builder;

    public Alert(Context context) {
        dialog = new ProgressDialog(context);
        builder = new AlertDialog.Builder(context);
    }

    public void startProgress(final String message) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    startProgress(message);
                }
            });
            return;
        }

        stopProgress();
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        isProgressWorking = true;
    }

    public void stopProgress() {
        if (isProgressWorking) {
            dialog.dismiss();
            isProgressWorking = false;
        }
    }

    public void show(final String message) {
        if (isProgressWorking) stopProgress();

        if (Looper.getMainLooper() != Looper.myLooper()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    show(message);
                }
            });
            return;
        }

        builder.setMessage(message)
        .setCancelable(false)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

}
