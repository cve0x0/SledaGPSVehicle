package eu.sleda.vehicle.helpers.camera;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;

import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.camera.interfaces.OnImageResult;
import eu.sleda.vehicle.objects.Size;

/**
 * Created by Cvetomir Marinov
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class Camera {

    private byte cameraType = CameraTypes.BACK;
    private CameraManager manager;
    private OnImageResult onImageResult;
    private CameraDevice camera;
    private CameraCaptureSession captureSession;
    private Size pictureSize = new Size(400, 400);
    private File output;
    private Handler backgroundHandler;

    public Camera(Context context, Handler backgroundHandler) {
        this.backgroundHandler = backgroundHandler;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        }
    }

    public void setCameraType(byte cameraType) {
        this.cameraType = cameraType;
    }

    public byte getCameraType() {
        return cameraType;
    }

    public void setPictureSize(Size pictureSize) {
        this.pictureSize = pictureSize;
    }

    @SuppressLint("NewApi")
    private void openCamera() {
        try {
            String cameraID = null;

            for (String camID: manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(camID);
                int cOrientation = characteristics.get(CameraCharacteristics.LENS_FACING);
                if((cOrientation == CameraCharacteristics.LENS_FACING_FRONT && cameraType == CameraTypes.FRONT)
                        || (cOrientation == CameraCharacteristics.LENS_FACING_BACK && cameraType == CameraTypes.BACK)) {
                    cameraID = camID;
                    break;
                }
            }
            Logger.d("Open camera: " + cameraID);

            if(cameraID == null) {
                onCaptureError();
                return;
            }

            manager.openCamera(cameraID, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice cameraDevice) {
                    Logger.d("opened");
                    camera = cameraDevice;
                    createCaptureSession();
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                    Logger.d("Disconnected");
                    cameraDevice.close();
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int i) {
                    Logger.d("err here");
                    onCaptureError();
                }
            }, backgroundHandler);
        } catch (CameraAccessException ex) {
            ex.printStackTrace();
        }
    }

    private CameraCaptureSession.CaptureCallback captureCallback
            = new CameraCaptureSession.CaptureCallback() {

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            closeCamera();
            onImageResult.onReady();
        }

    };

    private void createCaptureRequest(CaptureRequest previewRequest) {
        try {
            captureSession.capture(previewRequest, captureCallback, null);
        } catch (CameraAccessException ex) {
            Logger.e(Log.getStackTraceString(ex));
            onCaptureError();
        }
    }

    private void createCaptureSession() {
        Surface surface = createSurface();

        try {
            final CaptureRequest.Builder previewRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            previewRequestBuilder.addTarget(surface);

            camera.createCaptureSession(Collections.singletonList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Logger.d("configured");

                    captureSession = cameraCaptureSession;

                    previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                    createCaptureRequest(previewRequestBuilder.build());
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    onCaptureError();
                }
            }, null);
        } catch (Exception ex) {
            onImageResult.onError();
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private Surface createSurface() {
        ImageReader imageReader = ImageReader.newInstance(pictureSize.getWidth(), pictureSize.getHeight(), ImageFormat.JPEG, 1);
        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onImageAvailable(ImageReader imageReader) {
                    backgroundHandler.post(new ImageSaver(imageReader.acquireNextImage(), output, onImageResult));
                }
        }, backgroundHandler);

        return imageReader.getSurface();
    }

    private void closeCamera() {
        if(captureSession != null) {
            captureSession.close();
            captureSession = null;
        }

        if (camera != null) {
            camera.close();
            camera = null;
        }
    }

    private void onCaptureError() {
        closeCamera();
        onImageResult.onError();
        onImageResult.onReady();
    }

    public void takePicture(OnImageResult onImageResult, File output) {
        this.onImageResult = onImageResult;
        this.output = output;

        if (manager != null) {
            openCamera();
        }
    }

    private static class ImageSaver implements Runnable {
        private Image image;
        private File file;
        private OnImageResult onImageResult;

        ImageSaver(Image image, File file, OnImageResult onImageResult) {
            this.image = image;
            this.file = file;
            this.onImageResult = onImageResult;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        public void run() {
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();

            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);

            try {
                FileOutputStream output = new FileOutputStream(file);
                output.write(bytes);
                output.close();
                onImageResult.onResult(BitmapFactory.decodeByteArray(bytes , 0, bytes .length), file);
            } catch (IOException ex) {
                Logger.e(Log.getStackTraceString(ex));
                onImageResult.onError();
            }
        }
    }

}
