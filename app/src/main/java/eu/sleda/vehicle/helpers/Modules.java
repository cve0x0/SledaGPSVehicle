package eu.sleda.vehicle.helpers;

import java.util.Collection;
import java.util.HashMap;

import eu.sleda.vehicle.interfaces.Module;

/**
 * Created by Cvetomir Marinov
 */

public class Modules {
    private HashMap<String, Module> modules = new HashMap<>();

    public void add(String name, Module module) {
        modules.put(name, module);
    }

    public boolean exist(String name) {
        for (String n : modules.keySet()) {
            if (n.equals(name)) return true;
        }

        return false;
    }

    public void remove(String name) {
        modules.remove(name);
    }

    public Module get(String name) {
        return modules.get(name);
    }

    public Collection<Module> getModules() {
        return modules.values();
    }
}
