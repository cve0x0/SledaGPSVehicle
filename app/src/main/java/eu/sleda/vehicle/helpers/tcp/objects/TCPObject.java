package eu.sleda.vehicle.helpers.tcp.objects;

import java.lang.reflect.Field;

import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;

/**
 * Created by Cvetomir Marinov
 */

public class TCPObject {

    public void onObjectsWrite(EasyTCPClient conn) {
        Object instance = this;

        for (Field field : instance.getClass().getFields()) {
            try {
                Object value = field.get(instance);
                Class type = field.getType();

                if (type == Byte.class) {
                    conn.writeByte((byte) value);
                } else if (type == Short.class) {
                    conn.writeShort((short) value);
                } else if (type == Integer.class) {
                    conn.writeInt((int) value);
                } else if (type == Long.class) {
                    conn.writeLong((long) value);
                } else if (type == Float.class) {
                    conn.writeFloat((float) value);
                } else if (type == Double.class) {
                    conn.writeDouble((double) value);
                } else if (type == Character.class) {
                    conn.writeChar((char) value);
                } else if (type == String.class) {
                    conn.writeString((String) value);
                } else if (type == Boolean.class) {
                    conn.writeBoolean((boolean) value);
                } else if (type == TCPObject.class) {
                    ((TCPObject) value).onObjectsWrite(conn);
                }
            } catch (Exception ignored) {

            }

            if (!conn.isConnected()) break;
        }
    }

    public void onObjectsRead(EasyTCPClient conn) {
        Object instance = this;

        for (Field field : instance.getClass().getFields()) {
            try {
                Object value = null;
                Class type = field.getType();

                if (type == Byte.class) {
                    value = conn.readByte();
                } else if (type == Short.class) {
                    value = conn.readShort();
                } else if (type == Integer.class) {
                    value = conn.readInt();
                } else if (type == Long.class) {
                    value = conn.readLong();
                } else if (type == Float.class) {
                    value = conn.readFloat();
                } else if (type == Double.class) {
                    value = conn.readDouble();
                } else if (type == Character.class) {
                    value = conn.readChar();
                } else if (type == String.class) {
                    value = conn.readString();
                } else if (type == Boolean.class) {
                    value = conn.readBoolean();
                } else if (type == TCPObject.class) {
                    ((TCPObject) field.get(instance)).onObjectsRead(conn);
                }

                if (!conn.isConnected()) break;
                if (value != null) field.set(instance, value);
            } catch (Exception ignored) {

            }
        }
    }

}