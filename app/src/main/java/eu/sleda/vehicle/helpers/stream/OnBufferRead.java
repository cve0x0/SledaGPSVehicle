package eu.sleda.vehicle.helpers.stream;

/**
 * Created by Cvetomir Marinov
 */

public interface OnBufferRead {
    void onBufferRead(byte[] buf, int size);
}