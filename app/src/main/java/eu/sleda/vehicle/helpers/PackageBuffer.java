package eu.sleda.vehicle.helpers;

import android.util.SparseArray;

import java.io.ByteArrayOutputStream;

import eu.sleda.vehicle.helpers.stream.BufferStream;

/**
 * Created by Cvetomir Marinov
 */

public class PackageBuffer extends BufferStream {
    public SparseArray<Object> options = new SparseArray();
    private ByteArrayOutputStream out;
    public boolean requireResponse;

    public PackageBuffer() {
        this(new ByteArrayOutputStream());
    }

    public PackageBuffer(ByteArrayOutputStream out) {
        super(out);
        this.out = out;
        streamFlush(false);
    }

    public int size() {
        return out.size();
    }

    public byte[] toByteArray() {
        return out.toByteArray();
    }
}
