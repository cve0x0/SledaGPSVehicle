package eu.sleda.vehicle.helpers.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import eu.sleda.vehicle.helpers.stream.BufferStream;
import eu.sleda.vehicle.helpers.tcp.objects.TCPObject;
import eu.sleda.vehicle.helpers.tcp.streams.EasyInputZlib;
import eu.sleda.vehicle.helpers.tcp.streams.EasyOutputZlib;

/**
 * Created by Cvetomir Marinov
 */


public class EasyTCPClient extends BufferStream {
    private Socket socket;
    public static int defaultTimeout = 10*1000;

    public EasyTCPClient() {

    }

    public EasyTCPClient(Socket socket) {
        this(socket, false);
    }

    public EasyTCPClient(Socket socket, boolean ZLIB) {
        connect(socket, ZLIB);
    }

    public EasyTCPClient(String host, int port) {
        this(host, port, defaultTimeout);
    }

    public EasyTCPClient(String host, int port, int timeout) {
        this(host, port, timeout, false);
    }

    public EasyTCPClient(String host, int port, int timeout, boolean ZLIB) {
        connect(new InetSocketAddress(host, port), timeout, ZLIB);
    }

    public EasyTCPClient(InputStream in, OutputStream out) {
        super.init(in, out);
    }

    public boolean connect(String host, int port) {
        return connect(host, port, defaultTimeout);
    }

    public boolean connect(String host, int port, int timeout) {
        return connect(new InetSocketAddress(host, port), timeout);
    }

    public boolean connect(InetSocketAddress inet, int timeout) {
        return connect(inet, timeout, false);
    }

    public boolean connect(InetSocketAddress inet, int timeout, boolean ZLIB) {
        try {
            socket = new Socket();
            socket.connect(inet, timeout);
            socket = onSocket(socket);
            return connect(socket, ZLIB);
        } catch (IOException ignored) {
            return false;
        }
    }

    public boolean connect(Socket socket, boolean ZLIB) {
        try {
            this.socket = socket;

            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();

            if (ZLIB) {
                in = new EasyInputZlib(in);
                out = new EasyOutputZlib(out);
            }

            super.init(in, out);

            return true;
        } catch (IOException ignored) {
            return false;
        }
    }

    protected Socket onSocket(Socket socket) throws IOException {
        return socket;
    }

    public SocketAddress getRemoteAddress() {
        return socket.getRemoteSocketAddress();
    }

    public boolean isConnected() {
        return socket != null && !socket.isClosed();
    }

    @Override
    public void close() {
        super.close();

        try {
            if (socket != null)
                socket.close();
        } catch (Exception ignored) {

        }
    }

    public boolean writeObject(TCPObject obj) {
        obj.onObjectsWrite(this);
        return isConnected();
    }

    public Object readObject(Class c) {
        try {
            Object object = c.newInstance();
            if (object instanceof TCPObject)
                ((TCPObject) object).onObjectsRead(this);
            return object;
        } catch (Exception ex) {
            return null;
        }
    }

    public void setTimeout(int timeout) {
        try {
            socket.setSoTimeout(timeout);
        } catch (Exception ignored) {

        }
    }
}