package eu.sleda.vehicle.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import eu.sleda.vehicle.interfaces.OnDatabaseCreated;

/**
 * Created by Cvetomir Marinov
 */

public class SQLite extends SQLiteOpenHelper {
    private boolean isOpened = false;
    private OnDatabaseCreated onCreated = null;
    private SQLiteDatabase db;

    public SQLite(Context context, String db_name, int databaseVersion, OnDatabaseCreated onCreated) {
        super(context, db_name, null, databaseVersion);
        this.onCreated = onCreated;
        getWritableDatabase();
    }

    public void exec(String query) {
        exec(query, new String[] { });
    }

    public void exec(String query, String[] parms) {
        db.execSQL(query, parms);
    }

    public Cursor query(String query) {
        return query(query, null);
    }

    public Cursor query(String query, String[] parms) {
        return db.rawQuery(query, parms);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        isOpened = true;
        this.db = db;
        if(onCreated != null) {
            onCreated.onCreated(db);
        }
    }

    public long insert(String table, String nullColumnHack, ContentValues values) {
        return db.insert(table, nullColumnHack, values);
    }

    public void onOpen(SQLiteDatabase db) {
        isOpened = true;
        this.db = db;
    }

    public boolean isOpened() {
        return isOpened;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
