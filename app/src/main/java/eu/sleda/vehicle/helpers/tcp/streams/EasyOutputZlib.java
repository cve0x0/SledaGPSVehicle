package eu.sleda.vehicle.helpers.tcp.streams;

/**
 * Created by Cvetomir Marinov
 */

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;

public class EasyOutputZlib extends OutputStream {
    private OutputStream out;

    private byte[] inBuf = null;

    private byte[] outBuf = null;

    private int len = 0;

    private Deflater deflater = null;

    public EasyOutputZlib(OutputStream os)
            throws IOException {
        this(os, 1024);
    }
    public EasyOutputZlib(OutputStream os, int size)
            throws IOException {
        this(os, size,
                Deflater.DEFAULT_COMPRESSION, Deflater.DEFAULT_STRATEGY);
    }

    public EasyOutputZlib(OutputStream os, int size,
                          int level, int strategy) throws IOException {
        out = os;
        this.inBuf = new byte[size];
        this.outBuf = new byte[size + 64];
        this.deflater = new Deflater(level);
        this.deflater.setStrategy(strategy);
    }

    protected void compressAndSend() throws IOException {
        if (len > 0) {
            deflater.setInput(inBuf, 0, len);
            deflater.finish();
            int size = deflater.deflate(outBuf);

            out.write((size >> 24) & 0xFF);
            out.write((size >> 16) & 0xFF);
            out.write((size >>  8) & 0xFF);
            out.write((size) & 0xFF);

            out.write((len >> 24) & 0xFF);
            out.write((len >> 16) & 0xFF);
            out.write((len >>  8) & 0xFF);
            out.write((len) & 0xFF);

            out.write(outBuf, 0, size);
            out.flush();

            len = 0;
            deflater.reset();
        }
    }

    public void write(int b) throws IOException {
        inBuf[len++] = (byte) b;
        if (len == inBuf.length) {
            compressAndSend();
        }
    }

    public void write(byte[] b, int boff, int blen)
            throws IOException {
        while ((len + blen) > inBuf.length) {
            int toCopy = inBuf.length - len;
            System.arraycopy(b, boff, inBuf, len, toCopy);
            len += toCopy;
            compressAndSend();
            boff += toCopy;
            blen -= toCopy;
        }
        System.arraycopy(b, boff, inBuf, len, blen);
        len += blen;
    }

    public void flush() throws IOException {
        compressAndSend();
        out.flush();
    }

    public void close() throws IOException {
        compressAndSend();
        out.close();
    }
}
