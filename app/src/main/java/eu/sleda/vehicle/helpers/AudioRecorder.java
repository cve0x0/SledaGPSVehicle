package eu.sleda.vehicle.helpers;

import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.File;

import eu.sleda.vehicle.interfaces.OnTimeUpdate;

/**
 * Created by Cvetomir Marinov
 */

public class AudioRecorder {
    private String output;
    private OnTimeUpdate onTimeUpdate;
    private MediaRecorder recorder;
    private boolean isRecording;
    private byte minutes = 0, seconds = 0;

    public AudioRecorder(String output) {
        this.output = output;
        recorder = new MediaRecorder();

        recorder.setAudioSource(MediaRecorder.AudioSource.MIC); // Microphone
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4); // mp4 format
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC); // AAC encoding (.mp3 file)

        recorder.setOutputFile(output);

        isRecording = false;
    }

    // Start audio recorder
    public boolean start() {
        try {
            recorder.prepare();
            recorder.start();

            // On time update, check for reached limit
            if (onTimeUpdate != null) {
                new Thread() {
                    public void run() {
                        while (isRecording) {
                            if (seconds++ == 59) {
                                seconds = 0;
                                if (minutes++ == 59) minutes = 0;
                            }

                            if (onTimeUpdate != null) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        onTimeUpdate.onTimeUpdate(minutes, seconds);
                                    }
                                });
                            }

                            try {
                                Thread.sleep(1000);
                            } catch (Exception ignored) {

                            }
                        }
                    }
                }.start();
            }

            return (isRecording = true);
        } catch (Exception ex) {
            return false;
        }
    }

    public void setTimer(OnTimeUpdate onTimeUpdate) {
        this.onTimeUpdate = onTimeUpdate;
    }

    public File getFile() {
        return new File(output);
    }

    // Stop audio recorder
    public void stop() {
        isRecording = false;
        try {
            recorder.stop();
        } catch (Exception ex) {
            Logger.d(Log.getStackTraceString(ex));
        }
    }

    public boolean isRecording() {
        return isRecording;
    }
}
