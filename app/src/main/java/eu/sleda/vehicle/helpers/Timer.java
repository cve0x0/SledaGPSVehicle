package eu.sleda.vehicle.helpers;

/**
 * Created by Cvetomir Marinov
 */

public class Timer {
    private int millis;
    private long last;

    public Timer(int millis) {
        this(millis, true);
    }

    public Timer(int millis, boolean reset) {
        this.millis = millis;
        if (reset)
            reset();
    }

    public void reset() {
        last = System.currentTimeMillis();
    }

    public boolean isReady() {
        return System.currentTimeMillis() - last >= millis;
    }
}
