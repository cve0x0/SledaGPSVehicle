package eu.sleda.vehicle.helpers;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

/**
 * Created by Cvetomir Marinov
 */

public abstract class IntentService extends Service {
    private volatile Looper mServiceLooper;
    private volatile Handler mHandler;
    private String mName;

    public IntentService(String name) {
        super();
        mName = name;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread("IntentService[" + mName + "]");
        thread.start();

        mServiceLooper = thread.getLooper();
        mHandler = new Handler(mServiceLooper) {
            @Override
            public void handleMessage(Message message) {
                onMessage(message);
            }
        };
    }

    final public Handler getHandler() {
        return mHandler;
    }

    public void onMessage(Message message) {

    }

    @Override
    public int onStartCommand(@Nullable final Intent intent, int flags, int startId) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                onHandleIntent(intent);
            }
        });

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
    }

    @Override
    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    @WorkerThread
    protected abstract void onHandleIntent(@Nullable Intent intent);
}