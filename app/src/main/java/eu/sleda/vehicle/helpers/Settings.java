package eu.sleda.vehicle.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Cvetomir Marinov
 */

public class Settings {
    private SharedPreferences settings;
    private SharedPreferences.Editor preferancesEditor;

    public static final String
            FAST_MESSAGES_VERSION = "fast-messages-version",
            SOS_BUTTON = "SOSThread-button",
            SAVE_MESSAGES = "save-messages",
            MESSAGE_SOUND = "message-sound",
            MESSAGE_VIBRATE = "message-vibrate";

    public Settings(Context context) {
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        preferancesEditor = settings.edit();
    }

    public boolean getBool(String key) {
        boolean def = false;

        switch (key) {
            case SOS_BUTTON:
            case SAVE_MESSAGES:
            case MESSAGE_VIBRATE:
            case MESSAGE_SOUND:
                def = true;
                break;
        }

        return settings.getBoolean(key, def);
    }

    public int getInt(String key) {
        int def = -1;

        switch (key) {

        }

        return settings.getInt(key, def);
    }

    public Settings put(String key, Object value) {
        if (value instanceof String) {
            preferancesEditor.putString(key, (String) value);
        } else if (value instanceof Integer) {
            preferancesEditor.putInt(key, (Integer) value);
        } else if (value instanceof Long) {
            preferancesEditor.putLong(key, (Long) value);
        } else if (value instanceof Float) {
            preferancesEditor.putFloat(key, (Float) value);
        } else if (value instanceof Boolean) {
            preferancesEditor.putBoolean(key, (Boolean) value);
        } else {
            return this;
        }

        preferancesEditor.commit();

        return this;
    }
}
