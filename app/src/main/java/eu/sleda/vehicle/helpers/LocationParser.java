package eu.sleda.vehicle.helpers;

import android.text.SpannableString;
import android.text.Spanned;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.interfaces.OnLocationClick;
import eu.sleda.vehicle.objects.GEOLocation;
import eu.sleda.vehicle.objects.LocationClickSpan;

/**
 * Created by Cvetomir Marinov
 */

public class LocationParser {

    public static Matcher parse(String text) {
        Pattern pattern = Pattern.compile("(\\[l:)(.*?)(\\])");
        return pattern.matcher(text);
    }

    public SpannableString parse(String text, OnLocationClick cb) {
        if (text == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(text);
        Matcher matcher = parse(builder.toString());

        if (Watcher.getContext() == null) {
            return null;
        }

        String replace = Watcher.getContext().getString(R.string.location_parser_text);

        int start, end, new_end;

        HashMap<int[], ArrayList<GEOLocation>> locationsArr = new HashMap<>();

        while (matcher.find()) {
            if (matcher.groupCount() != 3) {
                continue;
            }

            ArrayList<GEOLocation> locations = new ArrayList<>();

            String data = matcher.group(2);

            for (String coordinate : data.split(",")) {
                String[] info = coordinate.split(";");

                if (info.length != 2) {
                    continue;
                }

                locations.add(new GEOLocation(Double.parseDouble(info[1]), Double.parseDouble(info[0])));
            }

            start = builder.indexOf(matcher.group(0));
            end = start + matcher.group(0).length();

            builder.replace(start, end, replace);

            new_end = start + replace.length();

            locationsArr.put(new int[]{start, new_end}, locations);
        }

        SpannableString result = new SpannableString(builder.toString());

        for (Map.Entry<int[], ArrayList<GEOLocation>> entry : locationsArr.entrySet()) {
            int[] position = entry.getKey();

            result.setSpan(new LocationClickSpan(entry.getValue(), cb), position[0], position[1], Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return result;
    }
}
