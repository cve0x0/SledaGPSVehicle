package eu.sleda.vehicle.helpers.stream;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Cvetomir Marinov
 */

public class BufferStream {
    private DataInputStream in;
    private DataOutputStream out;
    private int bufferSize = 1024;
    private int maxReadStringSize = 10 * 1024 * 1024; // 10 MB
    private long maxReadBytesSize = 100 * 1024 * 1024; // 100 MB
    private boolean streamFlush = true;
    private int chunkBufferSize = 255;
    public boolean err;
    public Exception ex;
    public final static byte FAILED_READ = -1;

    public BufferStream() {

    }

    public BufferStream(File file) {
        try {
            init(new FileInputStream(file), new FileOutputStream(file));
            streamFlush = false;
        } catch (Exception ignored) {

        }
    }

    public BufferStream(InputStream in, OutputStream out) {
        init(in, out);
    }

    public BufferStream(InputStream in) {
        init(in, null);
    }

    public BufferStream(OutputStream out) {
        init(null, out);
    }

    protected void init(InputStream in, OutputStream out) {
        err = false;

        if (in != null)
            this.in = new DataInputStream(in);
        if (out != null)
            this.out = new DataOutputStream(out);
    }

    public boolean isStreamFlushEnabled() {
        return streamFlush;
    }

    public void streamFlush(boolean streamFlush) {
        this.streamFlush = streamFlush;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public int getMaxReadStringSize() {
        return maxReadStringSize;
    }

    public void setMaxReadStringSize(int maxReadStringSize) {
        this.maxReadStringSize = maxReadStringSize;
    }

    public long getMaxReadBytesSize() {
        return maxReadBytesSize;
    }

    public void setMaxReadBytesSize(int maxReadBytesSize) {
        this.maxReadBytesSize = maxReadBytesSize;
    }

    public DataInputStream getInputStream() {
        return in;
    }

    public DataOutputStream getOutputStream() {
        return out;
    }

    protected byte onInputStreamError(Exception ex) {
        this.err = true;
        this.ex = ex;
        close();
        return FAILED_READ;
    }

    protected void onOutputStreamError(Exception ex) {
        this.err = true;
        this.ex = ex;
        close();
    }

    public void closeInputStream() {
        try {
            if (in != null) {
                in.close();
                in = null;
            }
        } catch (Exception ignored) {

        }
    }

    public void closeOutputStream() {
        try {
            if (out != null) {
                out.close();
                out = null;
            }
        } catch (Exception ignored) {

        }
    }

    public void close() {
        closeInputStream();
        closeOutputStream();
    }

    public int available() {
        try {
            return in.available();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public int skip() {
        try {
            int i = 0;

            while (in.available() > 0) {
                in.read();
                i++;
            }

            return i;
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public int read(byte[] buf, int offset, int size) {
        try {
            return in.read(buf, offset, size);
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public byte readByte() {
        try {
            return in.readByte();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public int readUnsignedByte() {
        try {
            return in.readUnsignedByte();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public short readShort() {
        try {
            return in.readShort();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public int readInt() {
        try {
            return in.readInt();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public long readLong() {
        try {
            return in.readLong();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public float readFloat() {
        try {
            return in.readFloat();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public double readDouble() {
        try {
            return in.readDouble();
        } catch (Exception ex) {
            return onInputStreamError(ex);
        }
    }

    public boolean readBoolean() {
        try {
            return in.readBoolean();
        } catch (Exception ex) {
            onInputStreamError(ex);
            return false;
        }
    }

    public char readChar() {
        try {
            return in.readChar();
        } catch (Exception ex) {
            onInputStreamError(ex);
            return (char) FAILED_READ;
        }
    }

    public boolean readBytes(OnBufferRead onBufferRead, long size, long maxReadBytesSize) {
        boolean overflow = size > maxReadBytesSize;
        if (size < 0 || overflow) {
            if (overflow)
                close();
            return false;
        }

        int r;
        int bufSize = size > bufferSize ? bufferSize : (int) size;
        byte[] buf = new byte[bufSize];
        while (size > 0) {
            bufSize = size > bufferSize ? bufferSize : (int) size;
            r = read(buf, 0, bufSize);
            if (err)
                return false;
            size -= r;
            onBufferRead.onBufferRead(buf, r);
        }

        return true;
    }

    public boolean readBytes(OnBufferRead onBufferRead, long maxReadBytesSize) {
        return readBytes(onBufferRead, readInt(), maxReadBytesSize);
    }

    public boolean readBytes(OnBufferRead onBufferRead) {
        return readBytes(onBufferRead, maxReadBytesSize);
    }

    public String readChunkString(int maxReadStringSize) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        int size;

        while (!err) {
            size = readUnsignedByte();

            maxReadStringSize -= size;
            if (err || maxReadStringSize < 0) {
                close();
                return null;
            }

            if (!readBytes(new OnBufferRead() {
                @Override
                public void onBufferRead(byte[] buf, int size) {
                    out.write(buf, 0, size);
                }
            }, size, size))
                return null;

            if (size < 255)
                break;
        }

        return !err ? out.toString() : null;
    }

    public String readChunkString() {
        return readChunkString(maxReadStringSize);
    }

    public String readLine(int maxReadStringSize) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        byte buffer;
        int i = 0;
        boolean overflow = false;

        while ((buffer = readByte()) != (byte) '\n' && buffer != (byte) '\r') {
            if (err || (overflow = (++i > maxReadStringSize))) {
                if (overflow)
                    close();
                return null;
            }

            out.write(buffer);
        }

        return out.toString();
    }

    public String readLine() {
        return readLine(maxReadStringSize);
    }

    public String readString(int maxReadStringSize) {
        int size = readInt();

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        if (!readBytes(new OnBufferRead() {
            @Override
            public void onBufferRead(byte[] buf, int size) {
                out.write(buf, 0, size);
            }
        }, size, maxReadStringSize))
            return null;

        return out.toString();
    }

    public String readString() {
        return readString(maxReadStringSize);
    }

    public boolean readFile(File file, long maxReadBytesSize) {
        if (!file.exists()) {
            try {
                if (!file.createNewFile())
                    return false;
            } catch (Exception ex) {
                return false;
            }
        } else if (!file.isFile() || !file.canWrite()) {
            return false;
        }

        try {
            final FileOutputStream fout = new FileOutputStream(file);
            long size = readLong();
            return readBytes(new OnBufferRead() {
                @Override
                public void onBufferRead(byte[] buf, int size) {
                    try {
                        fout.write(buf, 0, size);
                    } catch (Exception ignored) {

                    }
                }
            }, size, maxReadBytesSize);
        } catch (Exception ignored) {
            return false;
        }
    }

    public boolean readFile(File file) {
        return readFile(file, maxReadBytesSize);
    }

    public boolean flush() {
        try {
            out.flush();
            return true;
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean flushStream() {
        return !streamFlush || flush();
    }

    public boolean write(byte[] buf, int offset, int size) {
        try {
            out.write(buf, offset, size);
            return true;
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeByte(byte buf) {
        try {
            out.writeByte(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeShort(short buf) {
        try {
            out.writeShort(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeInt(int buf) {
        try {
            out.writeInt(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeLong(long buf) {
        try {
            out.writeLong(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeFloat(float buf) {
        try {
            out.writeFloat(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeDouble(double buf) {
        try {
            out.writeDouble(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeBoolean(boolean buf) {
        try {
            out.writeBoolean(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeChar(char buf) {
        try {
            out.writeChar(buf);
            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }

    public boolean writeBytes(byte[] buf) {
        return writeBytes(buf, buf.length);
    }

    public boolean writeBytes(byte[] buf, long size) {
        int offset = 0;
        long bufSize;

        while (offset < size) {
            bufSize = size - offset > bufferSize ? bufferSize : size - offset;
            if (!write(buf, offset, (int) bufSize))
                return false;
            offset += bufSize;
        }

        return flushStream();
    }

    public boolean writeChunkString(String buf) {
        byte[] buffer = buf.getBytes();
        int size = buffer.length;
        int offset = 0;
        int bufSize = 0;

        while (offset < size) {
            bufSize = size - offset > chunkBufferSize ? chunkBufferSize : (size - offset);
            if (!writeByte((byte) bufSize) || !write(buffer, offset, bufSize))
                return false;
            offset += bufSize;
        }

        return (bufSize == chunkBufferSize || size == 0) ? writeByte((byte) 0) : flushStream();
    }

    public boolean writeLine(String buf) {
        return writeBytes((buf + '\n').getBytes());
    }

    public boolean writeString(String buf) {
        byte[] buffer = buf.getBytes();
        return writeInt(buf.length()) && writeBytes(buffer);
    }

    public boolean writeFile(File file) {
        if (!file.isFile() || !file.canRead() || !writeLong(file.length()))
            return false;

        try {
            FileInputStream fin = new FileInputStream(file);

            int r;
            byte[] buf = new byte[bufferSize];
            while ((r = fin.read(buf)) > 0) {
                if (!write(buf, 0, r)) {
                    fin.close();
                    return false;
                }
            }

            return flushStream();
        } catch (Exception ex) {
            onOutputStreamError(ex);
            return false;
        }
    }
}