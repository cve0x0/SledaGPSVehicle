package eu.sleda.vehicle.helpers.camera;

/**
 * Created by Cvetomir Marinov
 */

public class CameraTypes {
    public static final byte BACK = 0x1;
    public static final byte FRONT = 0x2;
}
