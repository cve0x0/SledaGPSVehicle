package eu.sleda.vehicle.helpers.camera.interfaces;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Cvetomir Marinov
 */

public interface OnImageResult {
    void onResult(Bitmap image, File output);
    void onError();
    void onReady();
}
