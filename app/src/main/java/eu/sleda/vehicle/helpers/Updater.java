package eu.sleda.vehicle.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.Protocol;
import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class Updater {
    private Context context;
    private Alert alert;

    public Updater(Context context) {
        this.context = context;
        this.alert = new Alert(context);
    }

    public boolean isUpdateAvailable(int version) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return version > pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException ex) {
            Logger.e(Log.getStackTraceString(ex));
            return true;
        }
    }

    public boolean isUpdateRequired(int version) {
        return version % 10 == 0;
    }

    public void update(int version) {
        alert.startProgress(context.getString(R.string.updater_progress_text));

        File out = new File(Utils.createAppDirectory(Config.UPDATES_DIRECTORY), System.currentTimeMillis() + "-" + version + ".apk");
        EasyTCPClient conn = new EasyTCPClient(Config.SERVER, Config.PORT, Config.SOCKET_CONNECT_TIMEOUT * 1000);

        if (conn.isConnected()) {
            conn.setTimeout(Config.SOCKET_READ_TIMEOUT * 1000);
            if (conn.writeByte(Protocol.DOWNLOAD_UPDATE)) {
                if (conn.readFile(out)) {
                    alert.stopProgress();

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", out), "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    context.startActivity(intent);

                    return;
                }
            }
        }

        alert.show(context.getString(R.string.updater_download_error));
    }

}
