package eu.sleda.vehicle.helpers.tcp.interfaces;

import eu.sleda.vehicle.helpers.tcp.EasyTCPClient;

/**
 * Created by Cvetomir Marinov
 */

public interface OnClientAccept {
    void onAccept(EasyTCPClient conn);
}