package eu.sleda.vehicle.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;

import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.modules.ModuleMessages;
import eu.sleda.vehicle.objects.Message;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class SMSReceiver extends BroadcastReceiver {

    private static SMSReceiver instance;

    public SMSReceiver() {
        instance = this;
    }

    public static SMSReceiver getInstance() {
        return instance;
    }

    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        String smsFrom = null;

        long smsDate = 0;

        StringBuilder message = new StringBuilder();

        for (Object pdu : (Object[]) bundle.get("pdus")) {
            SmsMessage msg;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                String format = bundle.getString("format");

                msg = SmsMessage.createFromPdu((byte[]) pdu, format);
            } else {
                msg = SmsMessage.createFromPdu((byte[]) pdu);
            }

            message.append(msg.getMessageBody());
            smsFrom = msg.getOriginatingAddress();
            smsDate = msg.getTimestampMillis();
        }

        if (smsFrom == null || !smsFrom.equals(Config.PHONE)) return;

        Message msg = new Message(message.toString(), smsDate, MessagesTypes.MESSAGE_RECEIVED);
        msg.setIsSMS(true);

        ModuleMessages.instance().onMessageReceive(msg);

        abortBroadcast();
    }

}