package eu.sleda.vehicle.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Settings;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleHeadset;
import eu.sleda.vehicle.threads.SOSThread;

/**
 * Created by Cvetomir Marinov
 */

public class HeadsetReceiver extends BroadcastReceiver {
    private Watcher watcher = Watcher.getInstance();
    private Settings settings = watcher.getSettings();
    private boolean fistReceive = true;
    private static HeadsetReceiver instance;

    public HeadsetReceiver() {
        instance = this;
    }

    public static HeadsetReceiver getInstance() {
        return instance;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        byte state = (byte) intent.getIntExtra("state", 0);

        for (Module module : watcher.modules.getModules()) {
            if (module instanceof ModuleHeadset) ((ModuleHeadset) module).onHeadsetPlug(state);
        }

        if (!fistReceive && state == 1 && settings.getBool(Settings.SOS_BUTTON)) {
            new SOSThread(watcher).start();
        }

        fistReceive = false;
    }
}
