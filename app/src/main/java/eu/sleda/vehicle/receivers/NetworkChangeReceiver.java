package eu.sleda.vehicle.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleNetworkChange;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    private static int state = -1;
    private static NetworkChangeReceiver instance;

    public NetworkChangeReceiver(Context context) {
        instance = this;
        state = getState(context);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Logger.d("NetworkChangeReceiver::onReceive()");
        state = getState(context);

        Watcher watcher = Watcher.getInstance();

        if (watcher == null) {
            return;
        }

        for (Module module : watcher.modules.getModules()) {
            if (module instanceof ModuleNetworkChange) {
                ((ModuleNetworkChange) module).onNetworkChange(isNetworkAvailable(), state);
            }
        }
    }

    public static NetworkChangeReceiver getInstance() {
        return instance;
    }

    public int getState(Context context) {
        return Utils.getInternetConnectivityStatus(context);
    }

    public boolean isNetworkAvailable() {
        return state != -1;
    }

    public boolean isWiFi() {
        return state == ConnectivityManager.TYPE_WIFI;
    }

    public boolean isMobileNetwork() {
        return state == ConnectivityManager.TYPE_MOBILE;
    }
}