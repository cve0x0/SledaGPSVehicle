package eu.sleda.vehicle.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.services.AppLockService;
import eu.sleda.vehicle.utils.Utils;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.d("BootCompletedReceiver -> onReceive");

        if (Utils.isAppLockerActivated(context) && !Utils.isServiceRunning(context, AppLockService.class.toString())) {
            try {
                context.startService(new Intent(context, AppLockService.class));
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }
        }
    }
}
