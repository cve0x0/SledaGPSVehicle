package eu.sleda.vehicle.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleTemperature;

/**
 * Created by Cvetomir Marinov
 */

public class TemperatureReceiver extends BroadcastReceiver {
    private byte temperature = 0;
    private static TemperatureReceiver instance;

    public TemperatureReceiver() {
        instance = this;
    }

    public static TemperatureReceiver getInstance() {
        return instance;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        temperature = (byte) (intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10);
        Logger.d("TemperatureReceiver::onReceive() -> " + temperature);

        try {
            for (Module module : Watcher.getInstance().modules.getModules()) {
                if (module instanceof ModuleTemperature) {
                    ((ModuleTemperature) module).onTemperatureChange(temperature);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte getTemperature() {
        return temperature;
    }
}
