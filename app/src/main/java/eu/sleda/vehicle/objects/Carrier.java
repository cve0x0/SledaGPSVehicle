package eu.sleda.vehicle.objects;

/**
 * Created by Cvetomir Marinov
 */

public class Carrier {
    private int IMSI;
    private String name;

    public Carrier(int IMSI, String name) {
        this.IMSI = IMSI;
        this.name = name;
    }

    public int getIMSI() {
        return IMSI;
    }

    public String getName() {
        return name;
    }
}
