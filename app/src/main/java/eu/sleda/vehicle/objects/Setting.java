package eu.sleda.vehicle.objects;

import android.view.View;

import eu.sleda.vehicle.interfaces.SettingOnClick;
import eu.sleda.vehicle.interfaces.SettingOnSwitch;

/**
 * Created by Cvetomir Marinov
 */

public class Setting {
    private String title, description;
    private SettingOnClick onClick;
    private SettingOnSwitch onSwitch;
    private View view;

    public Setting(String title) {
        this(title, null);
    }

    public Setting(String title, String description) {
        this(title, description, null, null);
    }

    public Setting(String title, String description, SettingOnClick onClick) {
        this(title, description, onClick, null);
    }

    public Setting(String title, String description, SettingOnSwitch onSwitch) {
        this(title, description, null, onSwitch);
    }

    private Setting(String title, String description, SettingOnClick onClick, SettingOnSwitch onSwitch) {
        this.title = title;
        this.description = description;
        this.onClick = onClick;
        this.onSwitch = onSwitch;
    }

    public boolean isViewAvailable() {
        return view != null;
    }

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }

    public boolean isDescriptionAvailable() {
        return description != null;
    }

    public boolean isOnCickAvailable() {
        return onClick != null;
    }

    public boolean isOnSwitchAvailable() {
        return onSwitch != null;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public SettingOnClick getOnClick() {
        return onClick;
    }

    public SettingOnSwitch getOnSwitch() {
        return onSwitch;
    }
}
