package eu.sleda.vehicle.objects;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.sleda.vehicle.helpers.Logger;

/**
 * Created by Cvetomir Marinov
 */

public class FastMessage {

    private String message;
    private String text;
    private String action;
    private String data;

    public FastMessage(String message) {
        this.message = message;

        Pattern patternActionTag = Pattern.compile("^\\[a activity=\"([a-zA-Z._]+):?(.*)\"](.*)\\[/a]$");
        Matcher matcherActionTag = patternActionTag.matcher(message);

        if (matcherActionTag.find()) {
            this.action = matcherActionTag.group(1);

            this.data = matcherActionTag.group(2).trim();
            if (this.data.length() > 0) {
                this.data = '{' + this.data + '}';
            }

            this.text = matcherActionTag.group(3);
        } else {
            this.text = message;
        }
    }

    public String getMessage() {
        return message;
    }

    public String getText() {
        return this.text;
    }

    public String getAction() {
        return action;
    }

    public String getData() {
        return data;
    }

    public boolean haveAction() {
        return action != null;
    }

    public Message toMessage() {
        return new Message(this.text);
    }
}
