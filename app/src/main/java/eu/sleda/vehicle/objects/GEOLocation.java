package eu.sleda.vehicle.objects;

/**
 * Created by Cvetomir Marinov
 */

public class GEOLocation {

    private double latitude, longitude;

    public GEOLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}