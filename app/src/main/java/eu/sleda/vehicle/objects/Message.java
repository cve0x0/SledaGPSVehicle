package eu.sleda.vehicle.objects;

import android.view.View;

import java.io.File;

import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.interfaces.OnStatusChange;

/**
 * Created by Cvetomir Marinov
 */

public class Message {
    private String message;
    private long date, id;
    private int type;
    private boolean isSuccess;
    private File file;
    private byte fileType = MessagesTypes.MESSAGE_WITHOUT_FILE;
    private OnStatusChange onStatusChange;
    private boolean isSMS = false;
    private View view;

    public Message(String message) {
        this(message, System.currentTimeMillis());
    }

    public Message(String message, long date) {
        this(message, date, MessagesTypes.MESSAGE_SENT);
    }

    public Message(String message, long date, int type) {
        this(message, date, type, true);
    }

    public Message(String message, long date, int type, boolean isSuccess) {
        this(message, date, type, isSuccess, 0);
    }

    public Message(String message, long date, int type, boolean isSuccess, long ID) {
        this.message = message;
        this.date = date;
        this.type = type;
        this.isSuccess = isSuccess;
        this.id = ID;
    }

    public long getID() {
        return id;
    }

    public void setID(long ID) {
        this.id = ID;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isSMS() {
        return isSMS;
    }

    public void setIsSMS(boolean isSMS) {
        this.isSMS = isSMS;
    }

    public void setOnStatusChange(OnStatusChange onStatusChange) {
        this.onStatusChange = onStatusChange;
    }

    public void setSuccess(boolean isSuccess) {
        if(onStatusChange != null) {
            onStatusChange.onChange(isSuccess);
        }

        this.isSuccess = isSuccess;
    }

    public void setFile(File file, byte fileType) {
        this.file = file;
        this.fileType = fileType;
    }

    public boolean isViewAvailable() {
        return view != null;
    }

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }

    public byte getFileType() {
        return fileType;
    }

    public String getFilename() {
        return isFileImage() ? file.getName() : "";
    }

    public boolean isFileImage() {
        return fileType == MessagesTypes.MESSAGE_IMAGE;
    }

    public boolean isFileVoiceRecord() {
        return fileType == MessagesTypes.MESSAGE_VOICE;
    }

    public void removeFile() {
        file.delete();
        file = null;
    }

    public boolean isFileAvailable() {
        return fileType != MessagesTypes.MESSAGE_WITHOUT_FILE;
    }

    public File getFile() {
        return file;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public long getDate() {
        return date;
    }

}