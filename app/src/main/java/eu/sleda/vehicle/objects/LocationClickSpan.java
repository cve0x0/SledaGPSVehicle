package eu.sleda.vehicle.objects;

import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import java.util.ArrayList;

import eu.sleda.vehicle.R;
import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.interfaces.OnLocationClick;

/**
 * Created by Cvetomir Marinov
 */

public class LocationClickSpan extends ClickableSpan {

    private OnLocationClick listener;
    private ArrayList<GEOLocation> locations;

    public LocationClickSpan(ArrayList<GEOLocation> locations, OnLocationClick listener) {
        this.locations = locations;
        this.listener = listener;
    }

    @Override
    public void updateDrawState(final TextPaint textPaint) {
        textPaint.setColor(ContextCompat.getColor(Watcher.getContext(), R.color.colorPrimary));
    }

    @Override
    public void onClick(View widget) {
        listener.onClick(locations);
    }

}
