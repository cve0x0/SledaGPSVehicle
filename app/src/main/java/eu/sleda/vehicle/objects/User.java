package eu.sleda.vehicle.objects;

/**
 * Created by Cvetomir Marinov
 */

public class User {
    private String phone, token, firstname, lastname, company;
    private int userId;
    private short interval;

    public User(int userId, String phone, String token) {
        this.userId = userId;
        this.phone = phone;
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public String getPhone() {
        return phone;
    }

    public String getToken() {
        return token;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public short getInterval() {
        return this.interval;
    }

    public void setInterval(short interval) {
        this.interval = interval;
    }
}
