package eu.sleda.vehicle.objects;

/**
 * Created by Cvetomir Marinov
 */

public class Size {
    private int width, height;

    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
