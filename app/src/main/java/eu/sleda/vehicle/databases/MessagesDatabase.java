package eu.sleda.vehicle.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.ArrayList;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.configs.MessagesTypes;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.helpers.SQLite;
import eu.sleda.vehicle.interfaces.OnDatabaseCreated;
import eu.sleda.vehicle.objects.FastMessage;
import eu.sleda.vehicle.objects.LocationAddress;
import eu.sleda.vehicle.objects.Message;

/**
 * Created by Cvetomir Marinov
 */

public class MessagesDatabase extends SQLite {

    private Watcher watcher = Watcher.getInstance();

    public MessagesDatabase(Context context) {
        super(context, "messages.db", 1, new OnDatabaseCreated() {
            @Override
            public void onCreated(SQLiteDatabase database) {
                database.execSQL("CREATE TABLE fast_messages (id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT) ;");
                database.execSQL("CREATE TABLE messages (id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT, file TEXT DEFAULT NULL, fileType INTEGER DEFAULT " + MessagesTypes.MESSAGE_WITHOUT_FILE + ", date INTEGER, type INTEGER, isSuccess INTEGER, isSMS INTEGER, userID INTEGER) ;");
                database.execSQL("CREATE TABLE address_locations (id INTEGER PRIMARY KEY AUTOINCREMENT, userID INTEGER, latitude REAL, longitude REAL, address TEXT) ;");
            }
        });
    }

    public ArrayList<Message> getMessagesHistory(int h) {
        ArrayList<Message> history = new ArrayList<>();
        Cursor result = query("SELECT id, message, file, fileType, date, type, isSuccess, isSMS FROM messages WHERE userID = '" + watcher.user.getUserId() + "' AND date > " + (System.currentTimeMillis() - (h * 60 * 60 * 1000)) + " ;");

        if (result.moveToFirst()) {
            do {
                Message message = new Message(result.getString(1), result.getLong(4), result.getInt(5), result.getInt(6) == 1, result.getLong(0));

                byte fileType = (byte) result.getInt(3);
                if (fileType != MessagesTypes.MESSAGE_WITHOUT_FILE) {
                    message.setFile(new File(result.getString(2)), fileType);
                }

                message.setIsSMS(result.getInt(7) == 1);
                history.add(message);
            } while (result.moveToNext());
        }

        return history;
    }

    public ArrayList<Message> getMessagesBefore(int h) {
        ArrayList<Message> history = new ArrayList<>();
        Cursor result = query("SELECT id, message, file, fileType, date, type, isSuccess, isSMS FROM messages WHERE userID = '" + watcher.user.getUserId() + "' AND date < " + (System.currentTimeMillis() - (h * 60 * 60 * 1000)) + " ;");

        if (result.moveToFirst()) {
            do {
                Message message = new Message(result.getString(1), result.getLong(4), result.getInt(5), result.getInt(6) == 1, result.getLong(0));

                byte fileType = (byte) result.getInt(3);
                if (fileType != MessagesTypes.MESSAGE_WITHOUT_FILE) {
                    message.setFile(new File(result.getString(2)), fileType);
                }

                message.setIsSMS(result.getInt(7) == 1);
                history.add(message);
            } while (result.moveToNext());
        }

        return history;
    }

    // Update message in local device database, table "messages"
    public void updateMessage(Message message) {
        exec("UPDATE messages SET isSuccess = " + (message.isSuccess() ? 1 : 0) + " WHERE id = '" + message.getID() + "' ;");
    }

    // Delete messages from local device database, table "messages"
    public void clearMessages() {
        exec("DELETE FROM messages WHERE userID = '" + watcher.user.getUserId() + "' ;");
    }

    public void removeMessage(Message message) {
        exec("DELETE FROM messages WHERE userID = '" + watcher.user.getUserId() + "' AND id = '" + message.getID() + "' ;");
    }

    // Insert message into local device database, table "messages"
    public boolean writeMessage(Message message) {
        ContentValues values = new ContentValues();
        values.put("message", message.getMessage());

        if (message.isFileAvailable()) {
            values.put("file", message.getFile().getAbsolutePath());
            values.put("fileType", message.getFileType());
        }

        values.put("date", message.getDate());
        values.put("type", message.getType());
        values.put("isSuccess", message.isSuccess());
        values.put("isSMS", message.isSMS());
        values.put("userID", watcher.user.getUserId());

        long id = insert("messages", null, values);
        message.setID(id);

        return id != -1;
    }

    // Get fast messages from local device database, table "fast_messages"
    public void getFastMessages(ArrayList<FastMessage> messages) {
        Cursor result = query("SELECT message FROM fast_messages ;");

        if (result.moveToFirst()) {
            do {
                messages.add(new FastMessage(result.getString(0)));
            } while (result.moveToNext());
        }
    }

    // Delete fast messages from local device database, table "fast_messages"
    public void clearFastMessages() {
        exec("DELETE FROM fast_messages ;");
    }

    // Insert fast message into local device database, table "fast_messages"
    public void insertFastMessage(String message) {
        ContentValues values = new ContentValues();
        values.put("message", message);
        insert("fast_messages", null, values);
    }

    public void getAddressLocations(ArrayList<LocationAddress> locationAddressArrayList) {
        Cursor result = query("SELECT id, latitude, longitude, address FROM address_locations WHERE userID = '" + watcher.user.getUserId() + "' ;");

        if (result.moveToFirst()) {
            do {
                LocationAddress locationAddress = new LocationAddress(result.getInt(0), result.getDouble(1), result.getDouble(2), result.getString(3));
                locationAddressArrayList.add(locationAddress);
            } while (result.moveToNext());
        }
    }

    public void insertAddressLocation(LocationAddress locationAddress) {
        ContentValues values = new ContentValues();
        values.put("userID", watcher.user.getUserId());
        values.put("latitude", locationAddress.getLatitude());
        values.put("longitude", locationAddress.getLongitude());
        values.put("address", locationAddress.getAddress());

        locationAddress.setId((int) insert("address_locations", null, values));
    }

    public void removeAddressLocation(LocationAddress locationAddress) {
        exec("DELETE FROM address_locations WHERE userID = '" + watcher.user.getUserId() + "' AND id = '" + locationAddress.getId() + "' ;");
    }

}
