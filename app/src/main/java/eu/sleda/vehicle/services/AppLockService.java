package eu.sleda.vehicle.services;

import android.app.ActivityManager;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import eu.sleda.vehicle.activities.UnlockDialog;
import eu.sleda.vehicle.configs.Config;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.utils.Utils;

/**
 * Created by Cvetomir Marinov
 */

public class AppLockService extends Service {

    private static AppLockService instance;

    public ArrayList<String> whitelist = new ArrayList<>();
    private String lastPackageName = null;
    private ActivityManager activityManager;
    private String appPackageName;
    private WindowManager windowManager;
    private Timer timer;
    private ImageView imageView;
    private UnlockDialog dialog;

    public static AppLockService getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent  intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        SharedPreferences sharedPreferences = getSharedPreferences(this.getPackageName(), Context.MODE_PRIVATE);
        this.activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        this.appPackageName = this.getPackageName();
        this.windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        imageView = new ImageView(this);
        imageView.setVisibility(View.GONE);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        windowManager.addView(imageView, layoutParams);

        this.whitelist.add(this.appPackageName);
        this.whitelist.add(Utils.getLauncherPackageName(this));
        this.whitelist.add("com.android.systemui");
        this.whitelist.add("com.google.android.dialer");
        this.whitelist.add("com.google.android.apps.messaging");
        this.whitelist.add("com.android.camera");
        this.whitelist.add("com.android.camera.Camera");
        this.whitelist.add("com.google.android.GoogleCamera");
        this.whitelist.add("com.android.settings");
        this.whitelist.add("com.google.android.googlequicksearchbox");
        this.whitelist.add("com.android.gallery3d");
        this.whitelist.add("com.sygic.aura");
        this.whitelist.add("com.sygic.truck");

        if (!sharedPreferences.contains(Config.PREFERENCE_CODE_FIELD_KEY)) {
            return;
        }

        timer = new Timer("AppLockService");
        timer.schedule(updateTask, 1000L, 1000);
    }

    private TimerTask updateTask = new TimerTask() {
        @Override
        public void run() {
            String packageName = getLauncherTopApp();

            if (packageName == null || packageName.equals(appPackageName) || packageName.equals(lastPackageName)) {
                return;
            }

            boolean isDialogShow = isDialogShow();

            if (!whitelist.contains(packageName)) {
                if (!isDialogShow) {
                    showUnlockDialog();
                }
            } else if (isDialogShow) {
                hideUnlockDialog();
            }

            lastPackageName = packageName;
        }
    };

    private boolean isDialogShow() {
        return dialog != null && dialog.isShowing();
    }

    private void hideUnlockDialog() {
        try {
            imageView.post(new Runnable() {
                public void run() {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showUnlockDialog() {
        imageView.post(new Runnable() {
            public void run() {
                dialog = new UnlockDialog(getApplicationContext());
                dialog.show();
            }
        });
    }

    private String getLauncherTopApp() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            UsageStatsManager sUsageStatsManager = (UsageStatsManager) getApplicationContext().getSystemService(Context.USAGE_STATS_SERVICE);

            long endTime = System.currentTimeMillis();
            long beginTime = endTime - 10000;

            String result = null;

            UsageEvents.Event event = new UsageEvents.Event();
            UsageEvents usageEvents = sUsageStatsManager.queryEvents(beginTime, endTime);

            while (usageEvents.hasNextEvent()) {
                usageEvents.getNextEvent(event);

                if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                    result = event.getPackageName();
                }
            }

            if (!android.text.TextUtils.isEmpty(result)) {
                return result;
            }
        } else {
            List<ActivityManager.RunningTaskInfo> appTasks = this.activityManager.getRunningTasks(1);

            if (null != appTasks && !appTasks.isEmpty()) {
                return appTasks.get(0).topActivity.getPackageName();
            }
        }

        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (timer != null) {
            try {
                timer.cancel();
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }

            timer = null;
        }

        if (imageView != null) {
            try {
                windowManager.removeView(imageView);
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }
        }

        if (dialog != null) {
            try {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());

                e.printStackTrace();
            }
        }
    }
}

