package eu.sleda.vehicle.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModuleLocationListener;
import eu.sleda.vehicle.interfaces.ModuleSpeed;

/**
 * Created by Viliyan Vasilev, Cvetomir Marinov
 */

public class LocationTrackingService extends Service implements LocationListener {

    private static LocationTrackingService instance;

    private Watcher watcher = Watcher.getInstance();
    private ArrayList<Location> locationsArrayList = new ArrayList<>();
    private LocationManager locationManager;
    private String locationProvider;
    private Location lastKnownLocation;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10, MIN_TIME_BW_UPDATES = 15 * 1000;

    public Location getLocation() {
        return lastKnownLocation;
    }

    public boolean isLocationAvailable() {
        return lastKnownLocation != null;
    }

    public static LocationTrackingService instance() {
        return instance;
    }

    private void requestLocationUpdate() {
        if (locationManager == null) {
            return;
        }

        try {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationProvider = LocationManager.GPS_PROVIDER;
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationProvider = LocationManager.NETWORK_PROVIDER;
            } else {
                return;
            }

            Logger.d("Location Provider: " + locationProvider);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Logger.w("Permission not granted: " + Manifest.permission.ACCESS_FINE_LOCATION);
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Logger.w("Permission not granted: " + Manifest.permission.ACCESS_COARSE_LOCATION);
                }

                return;
            }
            locationManager.requestLocationUpdates(locationProvider, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

            if (lastKnownLocation != null) {
                onLocationChanged(lastKnownLocation);
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }
    }

    public ArrayList<Location> getLocationsHistory(boolean original) {
        return original ? locationsArrayList : new ArrayList<>(locationsArrayList);
    }

    public boolean hasSpeed() {
        return lastKnownLocation != null && lastKnownLocation.hasSpeed();
    }

    public int getSpeed() {
        return (int) lastKnownLocation.getSpeed() * 3600 / 1000;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        locationManager = (LocationManager) watcher.getSystemService(LOCATION_SERVICE);

        requestLocationUpdate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (locationManager != null) {
            try {
                locationManager.removeUpdates(this);
            } catch (Exception e) {
                Logger.e(e.getMessage());
                Logger.e(e.toString());
            }

            locationManager = null;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        lastKnownLocation = location;

        if (lastKnownLocation != null) {
            Logger.d("Location changed, latitude: " + lastKnownLocation.getLatitude() + "; longitude: " + lastKnownLocation.getLongitude());

            for (Module module : watcher.modules.getModules()) {
                if (module instanceof ModuleLocationListener) {
                    ((ModuleLocationListener) module).onLocationChange(lastKnownLocation);
                }

                if (module instanceof ModuleSpeed && hasSpeed()) {
                    ((ModuleSpeed) module).onSpeedChange(getSpeed());
                }
            }

            locationsArrayList.add(lastKnownLocation);
        }
    }

    @Override
    public void onStatusChanged(String mLocationProvider, int status, Bundle extras) {
        Logger.d("LocationTrackingService::onStatusChanged() -> " + status);
    }

    @Override
    public void onProviderEnabled(String locationProvider) {
        Logger.d("LocationTrackingService::onProviderEnabled() -> " + locationProvider);

        requestLocationUpdate();
    }

    @Override
    public void onProviderDisabled(String locationProvider) {
        Logger.d(locationProvider + "Provider Disabled");
        try {
            locationManager.removeUpdates(this);
        } catch (Exception e) {
            Logger.e(e.getMessage());
            Logger.e(e.toString());

            e.printStackTrace();
        }
    }
}
