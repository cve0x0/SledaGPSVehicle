package eu.sleda.vehicle.listeners;

import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;

import java.util.ArrayList;

import eu.sleda.vehicle.Watcher;
import eu.sleda.vehicle.helpers.Logger;
import eu.sleda.vehicle.interfaces.Module;
import eu.sleda.vehicle.interfaces.ModulePhoneStateChange;
import eu.sleda.vehicle.objects.Carrier;

/**
 * Created by Cvetomir Marinov
 */

public class StateListener extends PhoneStateListener {
    private int state = 0;
    private Carrier carrier = new Carrier(0, null);
    private static StateListener instance;
    private ArrayList<Carrier> blockList = new ArrayList<>();

    public StateListener() {
        instance = this;

        // Block mobile operators list (IMSI, Name)
        blockList.add(new Carrier(40001, "Azercell"));
        blockList.add(new Carrier(40002, "Bakcell"));
        blockList.add(new Carrier(40004, "Azerfon"));
        blockList.add(new Carrier(28201, "Geocell"));
        blockList.add(new Carrier(28202, "MagtiCom"));
        blockList.add(new Carrier(28204, "Beeline"));
        blockList.add(new Carrier(43211, "MCI"));
        blockList.add(new Carrier(43232, "Taliya"));
        blockList.add(new Carrier(41805, "Asiacell"));
        blockList.add(new Carrier(41820, "Zain"));
        blockList.add(new Carrier(41830, "Orascom"));
        blockList.add(new Carrier(41840, "Korek"));
        blockList.add(new Carrier(42501, "Partner/Orange"));
        blockList.add(new Carrier(42502, "Cellcom"));
        blockList.add(new Carrier(42503, "Pelephone"));
        blockList.add(new Carrier(42507, "HotMobile"));
        blockList.add(new Carrier(41603, "Umniah"));
        blockList.add(new Carrier(41677, "Orange(Mobilecom)"));
        blockList.add(new Carrier(40101, "Kartel"));
        blockList.add(new Carrier(40102, "Kcell"));
        blockList.add(new Carrier(40107, "Altel"));
        blockList.add(new Carrier(40177, "Tele2"));
        blockList.add(new Carrier(41501, "MIC1"));
        blockList.add(new Carrier(25003, "NCC"));
        blockList.add(new Carrier(25005, "Yenisey"));
        blockList.add(new Carrier(25007, "ZAO"));
        blockList.add(new Carrier(25015, "UFA-ZAO"));
        blockList.add(new Carrier(25017, "Uralsvyazinform(ERMAK)"));
        blockList.add(new Carrier(25035, "MOTIV"));
        blockList.add(new Carrier(2503500, "MOTIV"));
        blockList.add(new Carrier(2503501, "MOTIV"));
        blockList.add(new Carrier(2503502, "MOTIV"));
        blockList.add(new Carrier(2503503, "MOTIV"));
        blockList.add(new Carrier(25039, "Uralsvyazinform(URATEL)"));
        blockList.add(new Carrier(25054, "ZAO-Smarts(Tattelecom)"));
        blockList.add(new Carrier(90115, "On-Air"));
        blockList.add(new Carrier(42001, "STC"));
        blockList.add(new Carrier(42003, "Etihad"));
        blockList.add(new Carrier(41702, "MTN(Spacetel)"));
        blockList.add(new Carrier(24421, "Elisa"));
        blockList.add(new Carrier(52000, "TrueMove(Orange)"));
    }

    @Override
    public void onServiceStateChanged(ServiceState serviceState) {
        super.onServiceStateChanged(serviceState);
        Logger.d("StateListener::onServiceStateChanged() -> IMSI: " + serviceState.getOperatorNumeric());

        state = serviceState.getState();

        // Get IMSI
        String IMSI = serviceState.getOperatorNumeric();

        // If IMSI found, get mobile carrier name
        if (IMSI != null) {
            carrier = new Carrier(Integer.parseInt(IMSI), serviceState.getOperatorAlphaLong());
        } else {
            carrier = new Carrier(-1, null);
        }

        Watcher watcher = Watcher.getInstance();

        if (watcher != null) {
            for (Module module : watcher.modules.getModules()) {
                if (module instanceof ModulePhoneStateChange)
                    ((ModulePhoneStateChange) module).onStateChange(state, carrier);
            }
        }
    }

    public static StateListener getInstance() {
        return instance;
    }

    public int getState() {
        return state;
    }

    // Get mobile carrier (operator)
    public Carrier getCarrier() {
        return carrier;
    }

    // Is mobile carrier is from block list
    public boolean isBlocked() {
        for (Carrier carrier : blockList) {
            if (this.carrier.getIMSI() == carrier.getIMSI()) return true;
        }

        return false;
    }

    // Is connected to mobile carrier
    public boolean isConnected() {
        return carrier.getIMSI() > 0;
    }
}
