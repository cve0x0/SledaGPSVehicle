package eu.sleda.vehicle.configs;

/**
 * Created by Cvetomir Marinov
 */

public class Protocol {

    public final static byte
            NULL = 0x00,
            LOGIN = 0x01,
            LOGOUT = 0x02,
            LOGIN_SUCCESS = 0x03,
            LOGIN_INVALID = 0x04,
            LOGIN_TOKEN = 0x05,
            LOGIN_TOKEN_SUCCESS = 0x06,
            LOGIN_TOKEN_INVALID = 0x07,
            DOWNLOAD_UPDATE = 0x21,

            SEND_MESSAGES = 0x09,
            SEND_MESSAGE_WITHOUT_FILE = 0x0A,
            RECEIVE_MESSAGES = 0x0B,
            MESSAGES_RECEIVED_SUCCESS = 0x0C,
            SEND_LOCATIONS = 0x0D,
            SEND_DETAILS = 0x0E,
            SEND_GPS_SPEED = 0x0F,
            SEND_TEMPERATURE = 0x10,
            UPDATE_FAST_MESSAGES = 0x11,
            SEND_PACKAGE = 0x12,
            RECEIVE_PACKAGE_SUCCESS = 0x13;

}