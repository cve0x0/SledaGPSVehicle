package eu.sleda.vehicle.configs;

/**
 * Created by Cvetomir Marinov
 */

public class Config {

    public static String SERVER = "sleda.eu";
    public static int PORT = 4554;

    public static final short
            CONNECTION_TIMEOUT_INTERVAL = 5*60,
            REGULARLY_INTERVAL = 10;

    public static final byte
            SOCKET_READ_TIMEOUT = 10,
            SOCKET_CONNECT_TIMEOUT = 15;

    public static final String TAG = "SLEDA-TRACKER";

    public static final boolean
            DEBUG_LOG = true,
            VERBOSE_LOG = true,
            INFO_LOG = true,
            WARN_LOG = true,
            ERROR_LOG = true;

    public static final String PHONE = "+359894692997";

    public static final String
            HOME_DIRECTORY = ".SLEDA",
            TMP_DIRECTORY = "tmp",
            UPDATES_DIRECTORY = "updates";

    public static final String PREFERENCE_CODE_FIELD_KEY = "AppLockUnlockCode";
}
