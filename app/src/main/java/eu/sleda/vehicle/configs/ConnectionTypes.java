package eu.sleda.vehicle.configs;

/**
 * Created by Cvetomir Marinov
 */

public class ConnectionTypes {
    public static final byte NOT_AVAILABLE = 0x0; // No connection
    public static final byte WIFI = 0x1; // Connection is Wi-Fi
    public static final byte MOBILE = 0x2; // Cellular data connection
    public static final byte SMS = 0x3; // SMS communication
}
