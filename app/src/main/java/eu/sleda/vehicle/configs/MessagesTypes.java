package eu.sleda.vehicle.configs;

/**
 * Created by Cvetomir Marinov
 */

public class MessagesTypes {

    public static final int
            MESSAGE_SENT = 0x1, // Sent message
            MESSAGE_RECEIVED = 0x2; // Received message

    public static final byte
            MESSAGE_WITHOUT_FILE = 0x0, // Message without attached file
            MESSAGE_IMAGE = 0x1, // Message with attached file
            MESSAGE_VOICE = 0x2; // Voice message
}
